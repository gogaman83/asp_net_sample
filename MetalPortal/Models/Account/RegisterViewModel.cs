﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MetalPortal.Core.Models;

namespace MetalPortal.Models
{
    public class RegisterViewModel : PageViewModel
    {
        [Display(Name = "Ваше имя")]
        public string UserName { get; set; }
        [Display(Name ="Электронная почта")]
        public string Email { get; set; }
        [Display(Name = "Номер телефона")]
        public string Phone { get; set; }
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        [Display(Name = "Подтверждение пароля")]
        public string ConfirmPassword { get; set; }
        public string ReturnURL { get; set; }
        [Display(Name = "Покупатель / поставщик")]
        public UserRoles UserRole { get; set; }
        [Display(Name = "Организация")]
        public string Organization { get; set; }
        [Display(Name = "Адрес сайта")]
        public string OrganizationUrl { get; set; }
        [Display(Name = "Ссылка на прайс-лист")]
        public string PricelistUrl { get; set; }
        [Display(Name = "Код проверки")]
        public string VerificationCode { get; set; }

        public bool Success { get; set; }
        public bool PhoneVerification { get; set; }
        public bool ResendPhoneVerification { get; set; }

        public RegisterViewModel() : base("register")
        {

        }
    }
}
