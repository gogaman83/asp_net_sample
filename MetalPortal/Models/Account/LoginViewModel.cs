﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Models
{
    public class LoginViewModel : PageViewModel
    {
        [Display(Name = "Ваш email")]
        public string Email { get; set; }
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        public string ReturnURL { get; set; }

        public LoginViewModel() : base("login")
        {

        }
    }
}