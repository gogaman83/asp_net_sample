﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetalPortal.Models
{
    public class PageViewModel
    {
        public string PageId { get; set; }
        public string PageClass { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Guid { get; set; }
        public string Title { get; set; }
        public string H1 { get; set; }
        public List<BreadCrumb> BreadCrumbs { get; set; }
        public object Object { get; set; }
        public List<string> Classes { get; set; }

        public PageViewModel(string pageId)
        {
            this.PageId = pageId;
            this.PageClass = "m_page_" + this.PageId;
            this.Guid = Math.Abs(System.Guid.NewGuid().GetHashCode()).ToString();
            this.BreadCrumbs = new List<BreadCrumb>();
            this.Classes = new List<string>();
        }

        public PageViewModel(string pageId, string name, string url, List<string> classes = null) : this(pageId)
        {
            this.Name = name;
            this.Url = url;
            this.BreadCrumbs.Add(new BreadCrumb(name, url));
            if (classes != null)
            {
                this.Classes = classes;
                this.PageClass += " " + string.Join(" ", this.Classes);
            }
        }
    }

    public class BreadCrumb
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public bool Plain { get; set; }

        public BreadCrumb(string name, string url)
        {
            this.Name = name;
            this.Url = url;
        }
    }
}
