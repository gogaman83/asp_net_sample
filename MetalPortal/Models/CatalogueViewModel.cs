﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MetalPortal.Core;
using MetalPortal.Core.Models;

namespace MetalPortal.Models
{
    public class NavigationViewModel<T> : PageViewModel where T: EntityProduct
    {
        public NavigationResult<T> NavigationResult { get; set; }
        public List<T> Items { get; set; }
        public List<Order> Orders { get; set; }
        public List<Offer> Offers { get; set; }

        public NavigationViewModel(string name, string url) : base("navigation", name, url, new List<string> { (typeof(T)).Name })
        {
            this.Items = new List<T>();
            this.Orders = new List<Order>();
            this.Offers = new List<Offer>();
        }
    }
}