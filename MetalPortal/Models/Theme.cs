﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetalPortal.Models
{
    public class Theme
    {
        public string ControlBackColor { get; set; }
        public string ControlForeColor { get; set; }

        public Theme()
        {

        }
    }

    public class DefaultTheme : Theme
    {
        public DefaultTheme() : base()
        {
            this.ControlBackColor = "green";
            this.ControlForeColor = "white";
        }
    }
}
