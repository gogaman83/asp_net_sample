﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core
{
    public class Search
    {
        protected Globals globals { get; set; }
        protected string SearchString { get; set; }
        protected int Take { get; set; }

        public Search(Globals globals, string searchString, int take)
        {
            this.SearchString = searchString;
            this.Take = take;
            this.globals = globals;
        }

        public SearchResult Execute()
        {
            SearchResult result = new SearchResult() {
                SearchString = this.SearchString
            };

            var catalogueQuery = this.globals.db.Catalogue.Where(x => !x.Deleted);
            var vendorsQuery = this.globals.db.Users.Where(x => x.IsVendor);
            var offersQuery = this.globals.db.Offers.Where(x => !x.Deleted);
            var ordersQuery = this.globals.db.OrderItems.Where(x => !x.Deleted && !x.Order.Deleted);
            var advertsQuery = this.globals.db.Adverts.Where(x => !x.Deleted);

            var words = this.SearchString.Split(" ").Select(x => x.Trim().ToLower()).ToList();

            foreach (var word in words)
            {
                catalogueQuery = catalogueQuery.Where(x => x.Name.ToLower().StartsWith(word));

                vendorsQuery = vendorsQuery.Where(x => x.Name.ToLower().Contains(word) || x.Email.ToLower().Contains(word) || x.Organization.ToLower().Contains(word));

                var catalogueIds = this.globals.db.Catalogue.Where(x => !x.Deleted && x.Name.ToLower().StartsWith(word)).Select(x => x.Id).ToList();

                if (catalogueIds.Count > 0)
                {
                    offersQuery = words.Count > 1 ? offersQuery.Where(x => catalogueIds.Contains(x.CatalogueId)) : offersQuery.Where(x => false);
                    ordersQuery = ordersQuery.Where(x => catalogueIds.Contains(x.CatalogueId));
                    advertsQuery = advertsQuery.Where(x => catalogueIds.Contains(x.CatalogueItemId));
                }
                else
                {
                    offersQuery = offersQuery.Where(x => x.Size.ToLower() == word || x.SteelGrade.ToLower().Contains(word));
                    ordersQuery = ordersQuery.Where(x => x.Size.ToLower() == word || x.SteelGrade.ToLower().Contains(word));
                    advertsQuery = advertsQuery.Where(x => x.Title.ToLower().Contains(word) || x.Text.ToLower().Contains(word));
                }
            }

            result.Vendors = vendorsQuery.Take(this.Take).ToList();
            result.CatalogueItems = catalogueQuery.Take(this.Take).ToList();
            result.Offers = offersQuery.Take(this.Take).OrderByDescending(x => x.Id).Include(x => x.Catalogue).ToList();
            result.OrderItems = ordersQuery.Take(this.Take).OrderByDescending(x => x.Id).Include(x => x.Catalogue).ToList();
            result.Adverts = advertsQuery.Take(this.Take).OrderByDescending(x => x.Id).Include(x => x.CatalogueItem).ToList();

            return result;
        }
    }

    public class SearchResult
    {
        public string SearchString { get; set; }
        public List<User> Vendors { get; set; }
        public List<Offer> Offers { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public List<CatalogueItem> CatalogueItems { get; set; }
        public List<Advert> Adverts { get; set; }

        public List<int> VendorsIds {
            get
            {
                return this.Vendors.Select(x => x.Id).ToList();
            }
        }
        public List<int> OffersIds
        {
            get
            {
                return this.Offers.Select(x => x.Id).ToList();
            }
        }
        public List<int> OrderItemsIds
        {
            get
            {
                return this.OrderItems.Select(x => x.Id).ToList();
            }
        }
        public List<int> CatalogueItemsIds
        {
            get
            {
                return this.CatalogueItems.Select(x => x.Id).ToList();
            }
        }
        public List<int> AdvertsIds
        {
            get
            {
                return this.Adverts.Select(x => x.Id).ToList();
            }
        }

        public SearchResult()
        {

        }
    }
}
