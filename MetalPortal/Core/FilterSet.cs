﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using MetalPortal.Core.Models;

namespace MetalPortal.Core
{
    public abstract class FilterSet<T> where T : Entity
    {
        public List<Filter<T>> Filters { get; set; }

        public FilterSet()
        {
            this.Filters = new List<Filter<T>>();
        }

        public IQueryable<T> Apply(IQueryable<T> query)
        {
            foreach (var filter in this.Filters)
                query = filter.FilterQuery(query);

            return query;
        }
    }

    public class FilterSetProducts : FilterSet<Offer>
    {
        public FilterSetProducts(Globals globals) : base()
        {
            this.Filters.Add(new FilterBidProductType(globals));
            //this.Filters.Add(new FilterBidMetalType());
            this.Filters.Add(new FilterBidSteelGrade());
            //this.Filters.Add(new FilterBidArticle());
            this.Filters.Add(new FilterBidSize());
        }
    }
}
