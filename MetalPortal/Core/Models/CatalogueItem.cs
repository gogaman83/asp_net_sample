﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class CatalogueItem : EntityNamed
    {
        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public CatalogueItem Parent { get; set; }
        public string Image { get; set; }

        public CatalogueItem() : base()
        {

        }

        public CatalogueItem(string name) : base(name)
        {

        }

        public string Url
        {
            get
            {
                return this.Name.toURL();
            }
        }
    }
}
