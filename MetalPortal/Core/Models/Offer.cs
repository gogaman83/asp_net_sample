﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class Offer : EntityProduct
    {
        public string Article { get; set; }
        public string MetalType { get; set; }
        public double Quantity { get; set; }
        public string Comment { get; set; }
        public int? Price { get; set; }
        public int VendorId { get; set; }
        public int? PricelistId { get; set; }

        [ForeignKey("VendorId")]
        public User Vendor { get; set; }
        [ForeignKey("PricelistId")]
        public Pricelist Pricelist { get; set; }

        public Offer() : base()
        {

        }
    }
}
