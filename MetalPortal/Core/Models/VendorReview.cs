﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class VendorReview : Entity
    {
        public int VendorId { get; set; }
        public string AuthorName { get; set; }
        public int? AuthorId { get; set; }
        public string Message { get; set; }
        public int? RatingId;
        public bool Publish { get; set; }

        [ForeignKey("RatingId")]
        public VendorRating Rating { get; set; }
        [ForeignKey("AuthorId")]
        public User Author { get; set; }
        [ForeignKey("VendorId")]
        public User Vendor { get; set; }

        public VendorReview() : base()
        {

        }
    }
}
