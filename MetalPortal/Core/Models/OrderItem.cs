﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class OrderItem : EntityProduct
    {
        public int OrderId { get; set; }
        public int? OfferId { get; set; }

        public string Article { get; set; }
        public string MetalType { get; set; }
        public double Quantity { get; set; }

        [ForeignKey("OrderId")]
        public Order Order { get; set; }
        [ForeignKey("OfferId")]
        public Offer Offer { get; set; }

        public OrderItem() : base()
        {
        }
    }
}
