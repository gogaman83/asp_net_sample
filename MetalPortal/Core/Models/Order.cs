﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class Order : Entity
    {
        public int? UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public OrderStatuses Status { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public List<OrderItem> OrderItems { get; set; }
        public List<OrderRaising> Raisings { get; set; }

        [NotMapped]
        public bool CanBeRaised
        {
            get
            {
                return this.Raisings.Count < Globals.MaxRaisingNumber;
            }
        }

        [NotMapped]
        public long SortingOrder
        {
            get
            {
                DateTime lastRaisingDate = this.Raisings.Count > 0 ? this.Raisings.OrderBy(x => x.CreateDate).Last().CreateDate : DateTime.MinValue;

                return (DateTime.Now.Ticks - Math.Max((this.ChangeDate.HasValue ? this.ChangeDate.Value.Ticks : this.CreateDate.Ticks), lastRaisingDate.Ticks));
            }
        }

        public Order() : base()
        {
            this.OrderItems = new List<OrderItem>();
            this.Raisings = new List<OrderRaising>();
        }

        public override bool CanChange(Globals globals)
        {
            return (globals.user.IsAdmin || this.UserId == globals.user.Id);
        }
    }

    public enum OrderStatuses
    {
        [Display(Name = "Новый")]
        New = 0,
        [Display(Name = "Обработан")]
        Processed = 100,
    }
}
