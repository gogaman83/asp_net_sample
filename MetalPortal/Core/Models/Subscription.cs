﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace MetalPortal.Core.Models
{
    public class Subscription : PaidService
    {
        public SubscriptionPeriod Period { get; set; }

        public Subscription() : base()
        {
        }

        public override DateTime? GetExpireDate(DateTime startDate)
        {
            DateTime expireDate = new DateTime(startDate.Ticks);

            if (this.Period.Years.HasValue)
                expireDate = expireDate.AddYears(this.Period.Years.Value);
            if (this.Period.Months.HasValue)
                expireDate = expireDate.AddMonths(this.Period.Months.Value);

            return expireDate;
        }

        protected virtual bool IsActiveOnPeriod(DateTime startDate)
        {
            var expireDate = this.GetExpireDate(startDate);
            return expireDate.HasValue ? expireDate.Value > DateTime.Now : true;
        }

        public static Subscription CreateByTypeName(string typeName)
        {
            return (Subscription)Activator.CreateInstance(Type.GetType(typeName));
        }
    }

    public class PeriodSubscription : Subscription
    {
        public override bool IsActive(Globals globals, PaidServiceActivation serviceActivation, object obj = null)
        {
            return this.IsActiveOnPeriod(serviceActivation.CreateDate) && (obj == null || obj is Order);
        }
    }

    public class LimitSubscription : Subscription
    {
        public int Limit { get; set; }

        public override void Apply(Globals globals, PaidServiceActivation serviceActivation, object obj = null)
        {
            if (obj != null && obj is Order)
            {
                Order order = (Order)obj;
                if (!globals.db.LimitSubscriptionOrders.Any(x => x.ServiceActivationId == serviceActivation.Id && x.OrderId == order.Id))
                {
                    globals.db.LimitSubscriptionOrders.Add(new LimitSubscriptionOrder
                    {
                        OrderId = order.Id,
                        ServiceActivationId = serviceActivation.Id
                    });
                    globals.db.SaveChanges();
                }
            }
        }

        public override bool IsActive(Globals globals, PaidServiceActivation serviceActivation, object obj = null)
        {
            if (this.IsActiveOnPeriod(serviceActivation.CreateDate) && (obj == null || obj is Order))
            {
                return (obj != null && globals.db.LimitSubscriptionOrders.Any(x => x.ServiceActivationId == serviceActivation.Id && x.OrderId == ((Order)obj).Id)) || 
                       globals.db.LimitSubscriptionOrders.Count(x => x.ServiceActivationId == serviceActivation.Id) < this.Limit;
            }
            else
                return false;
        }
    }

    public struct SubscriptionPeriod
    {
        public int? Years { get; set; }
        public int? Months { get; set; }
    }
}
