﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace MetalPortal.Core.Models
{
    public class PaidServiceActivation : Entity
    {
        public int UserId { get; set; }
        public int? ServiceId { get; set; }
        public int? PaymentId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        [ForeignKey("ServiceId")]
        public PaidService Service { get; set; }
        [ForeignKey("PaymentId")]
        public Payment Payment { get; set; }

        public List<LimitSubscriptionOrder> LimitSubscriptionOrders { get; set; }

        [NotMapped]
        public bool IsActiveCalculated { get; set; }

        public PaidServiceActivation() : base()
        {
            this.LimitSubscriptionOrders = new List<LimitSubscriptionOrder>();
        }

        public void Apply(Globals globals, object obj = null)
        {
            this.Service.Apply(globals, this, obj);
        }

        public bool IsActive(Globals globals, object obj = null)
        {
            return this.Service.IsActive(globals, this, obj);
        }

        public DateTime? ExpireDate
        {
            get
            {
                return this.Service.GetExpireDate(this.CreateDate);
            }
        }
    }
}
