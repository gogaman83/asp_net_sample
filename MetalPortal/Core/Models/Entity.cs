﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MetalPortal.Core.Models
{
    public class Entity
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ChangeDate { get; set; }
        public bool Deleted { get; set; }

        public virtual void AfterInsert() { }
        public virtual void BeforeInsert() { }
        public virtual void AfterUpdate() { }
        public virtual void BeforeSave() { }
        public virtual void AfterSave() { }

        public virtual bool CanRead(Globals globals) {
            return true;
        }

        public virtual bool CanChange(Globals globals)
        {
            return true;
        }

        public virtual void Delete()
        {
            this.Deleted = true;
        }

        public Entity()
        {
            this.CreateDate = DateTime.Now;
        }

        public virtual void BeforeUpdate()
        {
            this.ChangeDate = DateTime.Now;
        }
    }

    public class EntityNamed : Entity
    {
        public string Name { get; set; }

        public EntityNamed() : base()
        {
        }

        public EntityNamed(string name) : this()
        {
            this.Name = name;
        }
    }

    public class EntityProduct : Entity
    {
        public string Size { get; set; }
        public string SteelGrade { get; set; }
        public int CatalogueId { get; set; }

        [ForeignKey("CatalogueId")]
        public CatalogueItem Catalogue { get; set; }

        public EntityProduct() : base()
        {
        }
    }
}
