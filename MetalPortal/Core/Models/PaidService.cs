﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace MetalPortal.Core.Models
{
    public class PaidService : Entity
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int PriceOld { get; set; }
        public string PriceText { get; set; }

        public PaidService() : base()
        {
        }

        public virtual void Apply(Globals globals, PaidServiceActivation serviceActivation, object obj = null)
        {
        }

        public virtual DateTime? GetExpireDate(DateTime startDate)
        {
            return null;
        }

        public virtual bool IsActive(Globals globals, PaidServiceActivation servieActivation, object obj = null)
        {
            return obj == null;
        }
    }
}
