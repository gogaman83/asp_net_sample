﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OfficeOpenXml;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace MetalPortal.Core.Models
{
    public class Pricelist : Entity
    {
        public int VendorId { get; set; }
        public int? UploadId { get; set; }
        public PricelistStatuses Status { get; set; }
        public string ErrorsJSON { get; set; }

        [ForeignKey("VendorId")]
        public User Vendor { get; set; }
        [ForeignKey("UploadId")]
        public Upload Upload { get; set; }

        [NotMapped]
        public List<Offer> Offers { get; set; }
        [NotMapped]
        public List<Dictionary<string, object>> OffersDrafts { get; set; }
        [NotMapped]
        public List<PricelistCellError> Errors { get; set; }
        [NotMapped]
        protected Globals globals { get; set; }

        public Pricelist() : base()
        {
            this.Offers = new List<Offer>();
            this.OffersDrafts = new List<Dictionary<string, object>>();
            this.Errors = new List<PricelistCellError>();
        }

        public Pricelist(Globals globals, User vendor) : base()
        {
            this.Vendor = vendor;
            this.globals = globals;
        }

        public void Serialize()
        {
            this.ErrorsJSON = this.Errors.toJSON();
        }

        public void Deserialize()
        {
            this.Errors = JsonConvert.DeserializeObject<List<PricelistCellError>>(this.ErrorsJSON);
        }

        public bool Load(IFormFile file)
        {
            this.Upload = this.globals.uploadsManager.UploadFile(file, "pricelists");
            return this.LoadUpload(this.Upload);
        }

        public async Task<bool> Load(string url)
        {
            this.Upload = await this.globals.uploadsManager.UploadUrl(url, "pricelists");
            return this.LoadUpload(this.Upload);
        }

        public bool Load(Dictionary<string, List<string>> columns)
        {
            List<List<string>> rows = new List<List<string>>();

            for (int rowIndex = 0; rowIndex < columns.Max(x => x.Value.Count); rowIndex++)
            {
                List<string> row = new List<string>();
                foreach (var column in columns)
                {
                    var value = column.Value.ElementAtOrDefault(rowIndex) != null ? column.Value[rowIndex].Trim() : "";
                    row.Add(value.IsNullOrEmpty() ? null : value);
                }
                rows.Add(row);
            }

            return this.LoadRows(rows);
        }

        protected bool LoadUpload(Upload upload)
        {
            List<List<string>> rows = new List<List<string>>();

            using (ExcelPackage excel = new ExcelPackage(new FileInfo(upload.FullPath)))
            {
                var worksheet = excel.Workbook.Worksheets[1];

                for (int rowIndex = 2; rowIndex <= worksheet.Dimension.Rows; rowIndex++)
                {
                    List<string> row = new List<string>();

                    for (int columnIndex = 1; columnIndex <= worksheet.Dimension.Columns; columnIndex++)
                    {
                        row[columnIndex - 1] = worksheet.Cells[rowIndex, columnIndex].Value.ToString();
                    }
                    rows.Add(row);
                }
            }

            return this.LoadRows(rows);
        }

        protected bool LoadRows(List<List<string>> rows)
        {
            this.OffersDrafts = new List<Dictionary<string, object>>();
            this.Errors = new List<PricelistCellError>();
            this.Offers = new List<Offer>();
            bool result = false;

            this.Status = PricelistStatuses.New;

            Dictionary<int, string> columnsMapping = new Dictionary<int, string> {
                { 1, "Catalogue" },
                { 2, "SteelGrade" },
                { 3, "MetalType" },
                { 4, "Size" },
                { 5, "Quantity" },
                { 6, "Price" },
                { 7 , "Article"},
                { 8 , "Comment"}
            };
            Dictionary<string, Func<object, object>> fieldsValidation = new Dictionary<string, Func<object, object>> {
                { "Catalogue", x => {
                    if (x == null)
                        throw new Exception("Заполните тип продукции");
                    return x;
                }},
                { "SteelGrade", x => {
                    if (x == null)
                        throw new Exception("Заполните марку стали");
                    return x;
                }},
                { "MetalType", x => {
                    return x;
                }},
                { "Size", x => {
                    if (x == null)
                        throw new Exception("Заполните размер");
                    return x;
                }},
                { "Quantity", x => {
                    if (x == null)
                        throw new Exception("Заполните количество");
                    try
                    {
                        return float.Parse(x.ToString().Replace(".", ","));
                    }
                    catch(Exception exception)
                    {
                        throw new Exception("Неверный формат количества");
                    }
                }},
                { "Price", x => {
                    if (x == null)
                        throw new Exception("Заполните цену");
                    try
                    {
                        return (int)float.Parse(x.ToString().Replace(".", ","));
                    }
                    catch(Exception exception)
                    {
                        throw new Exception("Неверный формат цены");
                    }
                }},
                { "Article", x => {
                    return x;
                }},
                { "Comment", x => {
                    return x;
                }},
            };

            for (int rowIndex = 0; rowIndex < rows.Count; rowIndex++)
            {
                var orderDraft = new Dictionary<string, object>();
                for (int columnIndex = 0; columnIndex < rows[rowIndex].Count; columnIndex++)
                {
                    if (columnsMapping.ContainsKey(columnIndex + 1))
                    {
                        var field = columnsMapping[columnIndex + 1];
                        try
                        {
                            orderDraft[field] = fieldsValidation[field](rows[rowIndex][columnIndex]);
                        }
                        catch (Exception exception)
                        {
                            this.Errors.Add(new PricelistCellError(rowIndex, columnIndex, exception.Message)
                            {
                                ColumnCode = field
                            });
                        }
                    }
                }
                this.OffersDrafts.Add(orderDraft);
            }

            if (this.Errors.Count == 0)
            {
                foreach (var draft in this.OffersDrafts)
                {
                    //Ищем предложение
                    var offer = this.globals.db.Offers
                        .Where(x => x.Catalogue.Name.Trim().ToLower() == draft["Catalogue"].ToString().ToLower())
                        .Where(x => x.SteelGrade.Trim().ToLower() == draft["SteelGrade"].ToString().ToLower())
                        .Where(x => x.Size.Trim().ToLower() == draft["Size"].ToString().ToLower())
                        .FirstOrDefault();

                    //Создаем предложение
                    if (offer == null)
                    {
                        //Ищем каталог
                        var catalogue = this.globals.db.Catalogue.Where(x => x.Name.Trim().ToLower() == draft["Catalogue"].ToString().ToLower()).FirstOrDefault();

                        //Добавляем каталог
                        if (catalogue == null)
                        {
                            catalogue = new CatalogueItem(draft["Catalogue"].ToString().ToLower());
                            this.globals.db.Catalogue.Add(catalogue);
                        }

                        //Добавляем предложение
                        offer = new Offer
                        {
                            Catalogue = catalogue,
                            Size = draft["Size"].NullableToString(),
                            SteelGrade = draft["SteelGrade"].NullableToString(),
                            Article = draft["Article"].NullableToString(),
                            Comment = draft["Comment"].NullableToString(),
                            MetalType = draft["MetalType"].NullableToString(),
                            Quantity = (float)draft["Quantity"],
                            Price = (int)draft["Price"],
                            Vendor = this.Vendor,
                            Pricelist = this
                        };
                        this.globals.db.Offers.Add(offer);
                    }
                    //Редактируем предложение
                    else
                    {
                        offer.Article = draft["Article"].ToString();
                        offer.Comment = draft["Comment"].ToString();
                        offer.MetalType = draft["MetalType"].ToString();
                        offer.Quantity = (float)draft["Quantity"];
                        offer.Price = (int)draft["Price"];
                        offer.Pricelist = this;
                    }

                    this.Offers.Add(offer);
                }

                this.Status = PricelistStatuses.Activated;

                //Добавляем прайс лист
                this.Serialize();
                this.globals.db.Pricelists.Add(this);

                //Сохраняем изменения
                this.globals.db.SaveChanges();

                result = true;
            }
            else
                result = false;

            return result;
        }
    }

    public enum PricelistStatuses
    {
        New = 0,
        Activated = 100
    }

    public class PricelistCellError
    {
        public int Row { get; set; }
        public int Cell { get; set; }
        public string ColumnCode { get; set; }
        public string Message { get; set; }

        public PricelistCellError()
        {

        }

        public PricelistCellError(int row, int cell, string messsage) : this()
        {
            this.Row = row;
            this.Cell = cell;
            this.Message = messsage;
        }

        public override string ToString()
        {
            List<string> strings = new List<string>();

            strings.Add("Ряд " + this.Row.ToString());
            if (this.Cell > 0)
                strings.Add(" : Колонка " + this.Cell.ToString());
            strings.Add(". " + this.Message);

            return string.Join("", strings);
        }

    }
}


