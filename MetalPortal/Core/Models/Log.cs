﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace MetalPortal.Core.Models
{
    public class Log : Entity
    {
        public LogTypes Type { get; set; }
        public string DataType { get; set; }
        public string Data { get; set; }

        public Log() : base()
        {
        }

        public Log(LogTypes type, object data) : base()
        {
            this.Type = type;
            this.DataType = data.GetType().FullName;
            this.Data = data.GetType() == typeof(System.String) ? data.ToString() : data.toJSON();
        }

        public Log(object data) : this(LogTypes.Other, data)
        {
        }

        public static void Add(Globals globals, object data)
        {
            globals.db.Log.Add(new Log(data));
            globals.db.SaveChanges();
        }
    }

    public enum LogTypes
    {
        Other = 0
    }

}
