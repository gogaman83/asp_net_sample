﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class OrderFeedback : Entity
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public string Text { get; set; }

        [ForeignKey("OrderId")]
        public Order Order { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

        public OrderFeedback() : base()
        {
        }
    }
}
