﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class LimitSubscriptionOrder : Entity
    {
        public int ServiceActivationId { get; set; }
        public int OrderId { get; set; }

        [ForeignKey("ServiceActivationId")]
        public PaidServiceActivation ServiceActivation { get; set; }
        [ForeignKey("OrderId")]
        public Order Order { get; set; }

        public LimitSubscriptionOrder() : base()
        {

        }
    }
}
