﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using Spire.Xls;

namespace MetalPortal.Core.Models
{
    public class Payment : Entity
    {
        public int UserId { get; set; }
        public int? ServiceId { get; set; }
        public float Amount { get; set; }
        public PaymentTypes Type { get; set; }
        public PaymentStatuses Status { get; set; }
        public DateTime? SetStatusDate { get; set; }
        public string ExternalId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        [ForeignKey("ServiceId")]
        public PaidService Service { get; set; }

        public Payment() : base()
        {
        }

        public void SetSuccess(Globals globals, string externalId = null)
        {
            this.Status = PaymentStatuses.Succeeded;
            this.ExternalId = externalId;
            this.SetStatusDate = DateTime.Now;

            var serviceActivation = globals.db.PaidServicesActivations.Where(x => x.PaymentId == this.Id).FirstOrDefault();

            if (serviceActivation == null && this.ServiceId.HasValue)
            {
                serviceActivation = new PaidServiceActivation
                {
                    UserId = this.UserId,
                    PaymentId = this.Id,
                    ServiceId = this.ServiceId
                };

                globals.db.PaidServicesActivations.Add(serviceActivation);
            }

            globals.db.SaveChanges();
        }

        public string SendAccount(Globals globals)
        {
            var userAccount = globals.db.UserAccountings.FirstOrDefault(x => x.UserId == this.UserId);
            var service = globals.db.PaidServices.Find(this.ServiceId);

            var dir = Directory.GetCurrentDirectory();

            var fileName = $"Счет №{this.Id}.pdf";
            var sourceFileName = globals.uploadsManager.RootPath + "/account.xlsx";
            var targetFileName = globals.uploadsManager.GetUploadsPath("accounts", true) + "/" + fileName;

            using (Workbook workbook = new Workbook())
            {
                workbook.LoadFromFile(sourceFileName);

                var sheet = workbook.ActiveSheet;

                string payerString = $"{userAccount.PayerName}, ИНН {userAccount.ITN}, {userAccount.BankName}, БИК {userAccount.BankId}, сч.№ {userAccount.PaymentAccount}";

                //sheet.Range["B5"].Text = userAccount.BankName;
                //sheet.Range["Z5"].Text = userAccount.BankId;
                //sheet.Range["Z6"].Text = userAccount.PaymentAccount;
                //sheet.Range["E8"].Text = userAccount.ITN;
                sheet.Range["B13"].Text = $"Счет на оплату №{this.Id} от {this.CreateDate.ToString("dd.mm.yyyy")}";
                sheet.Range["AB22"].Value = service.Price.ToString();
                sheet.Range["D22"].Text = service.Name;
                sheet.Range["G19"].Text = payerString;
                sheet.Range["B27"].Text = $"Всего наименований 1, на сумму {service.Price} рублей 00 копеек";
                sheet.Range["B28"].Text = service.PriceText + " рублей 00 копеек";

                workbook.SaveToFile(targetFileName, Spire.Xls.FileFormat.PDF);
                Mailer.SendEmail(userAccount.Email, $"Cчет на подписку \"{service.Name}\"", $"Cчет на подписку \"{service.Name}\"", new List<string> { targetFileName });
            }

            return globals.uploadsManager.GetUploadsLink("accounts", fileName);
        }
    }

    public enum PaymentTypes
    {
        [Display(Name = "Оплата онлайн")]
        Online = 0,
        [Display(Name = "Оплата банковского счета")]
        Bank = 1
    }

    public enum PaymentStatuses
    {
        [Display(Name = "Создан")]
        Created = 0,
        [Display(Name = "Ожидает оплаты")]
        Waiting = 10,
        [Display(Name = "Отклонен")]
        Rejected = 20,
        [Display(Name = "Отменен")]
        Cancelled = 30,
        [Display(Name = "Оплачен")]
        Succeeded = 100
    }

}
