﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace MetalPortal.Core.Models
{
    public class User : IdentityUser<int>
    {
        public string Name { get; set; }
        public UserRoles UserRole { get; set; }
        public UserStatuses Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string Organization { get; set; }
        public string Url { get; set; }
        public string PricelistUrl { get; set; }
        public bool IsMainVendor { get; set; }

        public List<PaidServiceActivation> ServiceActivations { get; set; }
        public UserAccounting UserAccounting { get; set; }

        public List<VendorRating> Ratings { get; set; }
        public List<VendorReview> Reviews { get; set; }

        public User() : base()
        {
            this.CreateDate = DateTime.Now;
            this.ServiceActivations = new List<PaidServiceActivation>();
            this.Ratings = new List<VendorRating>();
            this.Reviews = new List<VendorReview>();
        }
        public bool IsAdmin
        {
            get
            {
                return this.IsMainVendor;
            }
        }
        public bool IsVendor
        {
            get
            {
                return this.UserRole == UserRoles.Vendor;
            }
        }

        //Права
        public bool CanViewOrder(Globals globals, Order order)
        {
            return this.ServiceActivations.Any(x => x.IsActive(globals, order));
        }
        public bool CanRating(Globals globals)
        {
            return globals.user != null && !this.Ratings.Any(x => x.AuthorId == globals.user.Id);
        }
        public bool CanReview(Globals globals)
        {
            return globals.user != null && !this.Reviews.Any(x => x.AuthorId == globals.user.Id);
        }
    }

    public class IdentityUserRole : IdentityRole<int>
    {
    }

    public enum UserRoles
    {
        [Display(Name = "Покупатель")]
        Customer = 0,
        [Display(Name = "Поставщик")]
        Vendor = 10,
        Admin = 100
    }

    public enum UserStatuses
    {
        [Display(Name = "Новый")]
        New = 0,
        [Display(Name = "Подтвержденный")]
        Confirmed = 100
    }
}
