﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.IO;


namespace MetalPortal.Core.Models
{
    public class Advert : Entity
    {
        public int? UserId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Text { get; set; }
        public string Organization { get; set; }
        public AdvertTypes Type { get; set; }
        public int CatalogueItemId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        [ForeignKey("CatalogueItemId")]
        public CatalogueItem CatalogueItem { get; set; }

        public List<AdvertRaising> Raisings { get; set; }
        public List<Upload> Uploads { get; set; }

        [NotMapped]
        public bool CanBeRaised
        {
            get
            {
                return this.Raisings.Count < Globals.MaxRaisingNumber;
            }
        }

        [NotMapped]
        public long SortingOrder
        {
            get
            {
                DateTime lastRaisingDate = this.Raisings.Count > 0 ? this.Raisings.OrderBy(x => x.CreateDate).Last().CreateDate : DateTime.MinValue;

                return (DateTime.Now.Ticks - Math.Max((this.ChangeDate.HasValue ? this.ChangeDate.Value.Ticks : this.CreateDate.Ticks), lastRaisingDate.Ticks));
            }
        }

        public Advert() : base()
        {
            this.Raisings = new List<AdvertRaising>();
            this.Uploads = new List<Upload>();
        }

        public override bool CanChange(Globals globals)
        {
            return globals.user != null && (globals.user.IsAdmin || this.UserId == globals.user.Id);
        }

        public void UploadFiles(Globals globals, List<IFormFile> files)
        {
            foreach (var upload in globals.db.Uploads.Where(x => x.AdvertId == this.Id))
                upload.Delete();

            foreach (var file in files)
            {
                var upload = globals.uploadsManager.UploadFile(file, "adverts");
                upload.AdvertId = this.Id;
            }
            globals.db.SaveChanges();
        }
    }

    public enum AdvertTypes
    {
        [Display(Name = "продажа")]
        Sale = 0,
        [Display(Name = "покупка")]
        Buy = 1,
        [Display(Name = "предложение услуг")]
        SaleService = 2,
        [Display(Name = "спрос услуг")]
        BuyService = 3,
        [Display(Name = "другое")]
        Other = 99,
    }
}
