﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace MetalPortal.Core.Models
{
    public class Upload : Entity
    {
        public string Name { get; set; }
        public string Path { get; set; }

        public int? AdvertId { get; set; }
        [ForeignKey("AdvertId")]
        public Advert Advert { get; set; }

        public Upload() : base()
        {
        }

        public Upload(string name, string path) : base()
        {
            this.Name = name;
            this.Path = path;
        }

        public string ExternalPath
        {
            get
            {
                return string.Join("/", this.Path.Split("/").Skip(2));
            }
        }

        public string FullPath
        {
            get
            {
                return Directory.GetCurrentDirectory() + this.Path;
            }
        }

    }
}
