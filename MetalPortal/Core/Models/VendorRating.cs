﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class VendorRating : Entity
    {
        public int VendorId { get; set; }
        public int Rating { get; set; }
        public int? AuthorId { get; set; }
        
        [ForeignKey("AuthorId")]
        public User Author { get; set; }
        [ForeignKey("VendorId")]
        public User Vendor { get; set; }

        public VendorRating() : base()
        {

        }
    }
}
