﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Core.Models
{
    public class UserAccounting : Entity
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string PaymentAccount { get; set; }
        public string BankId { get; set; }
        public string BankName { get; set; }
        public string ITN { get; set; }
        public string PayerName { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public UserAccounting() : base()
        {

        }
    }
}
