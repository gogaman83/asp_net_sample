﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using MetalPortal.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MetalPortal.Core
{
    public class ApplicationContext : IdentityDbContext<User, IdentityUserRole, int>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        //Список таблиц в БД
        public DbSet<CatalogueItem> Catalogue { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderRaising> OrderRaisings { get; set; }
        public DbSet<OrderFeedback> OrderFeedbacks { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Upload> Uploads { get; set; }
        public DbSet<Pricelist> Pricelists { get; set; }
        public DbSet<Advert> Adverts { get; set; }
        public DbSet<AdvertRaising> AdvertRaisings { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaidServiceActivation> PaidServicesActivations { get; set; }
        public DbSet<LimitSubscriptionOrder> LimitSubscriptionOrders { get; set; }
        public DbSet<UserAccounting> UserAccountings { get; set; }
        public DbSet<VendorRating> VendorRatings { get; set; }
        public DbSet<VendorReview> VendorReviews { get; set; }

        public DbSet<PaidService> PaidServices { get; set; }
        public DbSet<PeriodSubscription> PeriodSubscriptions { get; set; }
        public DbSet<LimitSubscription> LimitSubscriptions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Offer>().HasIndex(b => b.Article);
            modelBuilder.Entity<Offer>().HasIndex(b => b.Size);
            modelBuilder.Entity<Offer>().HasIndex(b => b.SteelGrade);

            modelBuilder.Entity<Subscription>()
                .Property(x => x.Period)
                .HasConversion(
                    x => x.toJSON(),
                    x => JsonConvert.DeserializeObject<SubscriptionPeriod>(x)
                );

            modelBuilder
                .Entity<User>()
                .HasMany(x => x.Ratings)
                .WithOne(x => x.Vendor);

            modelBuilder
                .Entity<User>()
                .HasMany(x => x.Reviews)
                .WithOne(x => x.Vendor);

            //modelBuilder.Entity<User>()
            //    .Property(x => x.Subscription)
            //    .HasConversion(
            //        x => x.GetType().FullName,
            //        x => Subscription.CreateByTypeName(x)
            //    );
        }

        public override int SaveChanges()
        {
            //Получаем все изменяемые сущности
            var insertedEntities = ChangeTracker.Entries().Where(x => x.State == EntityState.Added).Where(x => x.Entity.GetType().IsSubclassOf(typeof(Entity))).Select(x => (Entity)x.Entity).ToList();
            var updatedEntities = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified).Where(x => x.Entity.GetType().IsSubclassOf(typeof(Entity))).Select(x => (Entity)x.Entity).ToList();

            var tt = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified).ToList();

            //До сохранения
            foreach (var insertedEntity in insertedEntities)
            {
                insertedEntity.BeforeInsert();
                insertedEntity.BeforeSave();
            }
            foreach (var updatedEntity in updatedEntities)
            {
                updatedEntity.BeforeUpdate();
                updatedEntity.BeforeSave();
            }

            //Сохраняем
            var result = base.SaveChanges();

            //После сохранения
            foreach (var insertedEntity in insertedEntities)
            {
                insertedEntity.AfterInsert();
                insertedEntity.AfterSave();
            }
            foreach (var updatedEntity in updatedEntities)
            {
                updatedEntity.AfterUpdate();
                updatedEntity.AfterSave();
            }

            return result;
        }
    }
}
