﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MetalPortal.Core
{
    public class Utils
    {
        public static string FormatUrl(string url)
        {
            return url.NotNullOrEmpty() ? (new UriBuilder(url)).Uri.AbsoluteUri : "";
        }
        public static bool IsValidUrl(string url)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                var response = (HttpWebResponse)request.GetResponse();
                return response.StatusCode == HttpStatusCode.OK;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public static string ClearPhone(string phone)
        {
            return "+7" + (new string(phone.Where(c => char.IsDigit(c)).TakeLast(10).ToArray()));
        }
        public static bool IdValidPhone(string phone)
        {
            return Utils.ClearPhone(phone).Length == 12;
        }
        public static string FormatPhone(string phone)
        {
            return phone.Substring(0, 2) + " (" + phone.Substring(2, 3) + ") " + phone.Substring(5, 3) + "-" + phone.Substring(8, 2) + "-" + phone.Substring(10, 2);
        }
    }
}
