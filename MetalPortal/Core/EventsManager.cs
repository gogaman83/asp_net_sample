﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace MetalPortal.Core
{
    public class EventsManager
    {
        protected Globals globals { get; set; }

        public EventsManager(Globals globals)
        {
            this.globals = globals;
        }

        public void Dispatch (EventTypes eventType, object obj)
        {
            if (eventType == EventTypes.ViewOrder && obj is Order)
            {
                var currentActivation = this.globals.user.ServiceActivations.Where(x => x.IsActive(this.globals, obj)).OrderBy(x => x.ExpireDate).FirstOrDefault();
                currentActivation.Apply(this.globals, obj);
            }
        }
    }

    public enum EventTypes
    {
        ViewOrder = 0
    }
}
