﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using MetalPortal.Core.Models;

namespace MetalPortal.Core
{
    public abstract class Filter<T> where T : Entity
    {
        public Type OptionType { get; set; }
        public object Value { get; set; }
        public string PropertyName { get; set; }
        public string Title { get; set; }
        public string Placeholder { get; set; }

        public virtual IQueryable<T> FilterQuery(IQueryable<T> query)
        {
            return query;
        }
        public virtual List<EntityNamed> GetOptions()
        {
            return new List<EntityNamed>();
        }
    }

    public abstract class ListFilter<T, S> : Filter<T> where T : Entity where S: EntityNamed
    {
        protected Globals globals { get; set; }

        public ListFilter(Globals globals) : base()
        {
            this.Placeholder = "Выберите значение";
            this.OptionType = typeof(S);
            this.globals = globals;
        }
        public override IQueryable<T> FilterQuery(IQueryable<T> query)
        {
            return this.Value != null ? query.Where(this.PropertyName + "==" + this.Value) : query;
        }

        public override List<EntityNamed> GetOptions()
        {
            return globals.db.Set<S>().ToList().Select(x => x as EntityNamed).OrderBy(x => x.Name).ToList();
        }
    }

    public abstract class StringFilter<T> : Filter<T> where T : Entity
    {
        public StringFilter() : base()
        {
            this.OptionType = typeof(string);
            this.Placeholder = "Введите значение";
        }
    }

    //Product
    public class FilterBidProductType : ListFilter<Offer, CatalogueItem>
    {
        public FilterBidProductType(Globals globals) : base(globals)
        {
            this.Title = "Тип продукции";
            this.PropertyName = "CatalogueId";
        }
    }

    public class FilterBidMetalType : StringFilter<Offer>
    {
        public FilterBidMetalType() : base()
        {
            this.Title = "Тип металла";
            this.PropertyName = "MetalType";
        }
    }

    public class FilterBidSteelGrade : StringFilter<Offer>
    {
        public FilterBidSteelGrade() : base()
        {
            this.Title = "Марка стали";
            this.PropertyName = "SteelGrade";
        }
        public override IQueryable<Offer> FilterQuery(IQueryable<Offer> query)
        {
            return this.Value != null ? query.Where(x => x.SteelGrade.ToLower().Contains(this.Value.ToString().ToLower())) : query;
        }
    }

    public class FilterBidArticle : StringFilter<Offer>
    {
        public FilterBidArticle() : base()
        {
            this.Title = "Артикул";
            this.PropertyName = "Article";
        }
        public override IQueryable<Offer> FilterQuery(IQueryable<Offer> query)
        {
            return this.Value != null ? query.Where(x => x.Article.ToLower().Contains(this.Value.ToString().ToLower())) : query;
        }
    }

    public class FilterBidSize : StringFilter<Offer>
    {
        public FilterBidSize() : base()
        {
            this.Title = "Размер";
            this.PropertyName = "Size";
        }
        public override IQueryable<Offer> FilterQuery(IQueryable<Offer> query)
        {
            return this.Value != null ? query.Where(x => x.Size.ToLower().Contains(this.Value.ToString().ToLower())) : query;
        }
    }

}
