﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using UnidecodeSharpFork;
using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace MetalPortal.Core
{
    public class Globals
    {
        //Dynamic
        public ApplicationContext db;
        public UserManager<User> userManager;
        public SignInManager<User> signInManager;
        public IHttpContextAccessor contextAccessor;
        public User user;
        public UploadsManager uploadsManager;
        public IServiceCollection services;
        public IConfiguration configuration;
        public EventsManager eventsManager;

        //Static
        public const string ApplicationDomain = "mpruda.ru";
        public const string ApplicationAnchor = "<a href=\"http://mpruda.ru/\">mpruda.ru</a>";
        public const string ApplicationName = "Металличесий портал \"РУДА\"";
        public const int MaxRaisingNumber = 3;
        public const string LongDateTimeFormat = "dd MMMM yyyy hh:mm";
        public const string ShortDateFormat = "dd MMMM yyyy";

        public Globals(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext applicationContext, IHttpContextAccessor contextAccessor, UploadsManager uploadsManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.db = applicationContext;
            this.contextAccessor = contextAccessor;
            if (contextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                this.user = userManager.FindByNameAsync(contextAccessor.HttpContext.User.Identity.Name).Result;
                this.user.ServiceActivations = applicationContext.PaidServicesActivations.Where(x => x.UserId == this.user.Id).Include(x => x.Service).ToList();
                this.user.UserAccounting = applicationContext.UserAccountings.Where(x => x.UserId == this.user.Id).FirstOrDefault();
            }
            this.uploadsManager = uploadsManager;
            this.configuration = configuration;
            this.eventsManager = new EventsManager(this);
        }

        public static string CurrentVersionHash
        {
            get
            {
                return DateTime.Now.GetHashCode().ToString();
            }
        }

        public static string GenerateRandomString(int stringLength, string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789")
        {
            Random rd = new Random();
            char[] chars = new char[stringLength];

            for (int i = 0; i < stringLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return "1111";
            //return new string(chars);
        }
    }

    public static class Extensions
    {
        public static bool IsNullOrEmpty(this string str)
        {
            return (str == null || str == "");
        }
        public static bool NotNullOrEmpty(this string str)
        {
            return !str.IsNullOrEmpty();
        }
        public static T GetAttribute<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }
        public static string DisplayName(this Enum enumVal)
        {
            string result = string.Empty;

            var displayAttribute = enumVal.GetAttribute<DisplayAttribute>();
            if (displayAttribute != null)
                result = enumVal.GetAttribute<DisplayAttribute>().Name;
            else
                result = enumVal.ToString();

            return result;
        }
        public static int ToInt(this Enum enumVal)
        {
            return Convert.ToInt32(enumVal);
        }
        public static string toJSON(this object data)
        {
            return data.toJSON(true, true);
        }
        public static string toJSON(this object data, bool onlyWritable = true, bool camelCase = true)
        {
            var settings = new JsonSerializerSettings();

            if (camelCase)
                settings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();

            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            return JsonConvert.SerializeObject(data, Formatting.None, settings);
        }
        public static string EnumToJson(this Type enumType)
        {
            var enumList = enumType.EnumToList();
            return enumList.toJSON(camelCase: false);
        }
        public static List<EnumListItem> EnumToList(this Type enumType)
        {
            List<EnumListItem> enumList = new List<EnumListItem>();
            foreach (var item in Enum.GetValues(enumType))
            {
                Enum enumItem = (Enum)item;
                var displayName = enumItem.DisplayName();
                if (displayName.IsNullOrEmpty())
                    displayName = enumItem.ToString();
                string icon = null;
                var iconAttribute = enumItem.GetAttribute<IconAttribute>();
                if (iconAttribute != null)
                    icon = iconAttribute.Path;
                enumList.Add(new EnumListItem { Id = (int)item, Name = enumItem.ToString(), Description = displayName, Icon = icon, IconObject = iconAttribute });
            }
            return enumList;
        }
        public static string toCamelCase(this string data)
        {
            int index = 0;
            foreach (Char c in data)
            {
                if (Char.IsUpper(c))
                    break;
                index++;
            }
            return index == 0 ? (data.Substring(0, 1).ToLowerInvariant() + data.Substring(1)) : (data.Substring(0, index).ToLowerInvariant() + data.Substring(index));
        }
        public static string toURL(this string data)
        {
            return string.Join("_", data.Trim().Replace(",",".").Unidecode().Split(" "));
        }
        public static string ToShortDate(this DateTime dateTime)
        {
            return dateTime.ToString(Globals.ShortDateFormat);
        }
        public static string ToLongDateTime(this DateTime dateTime)
        {
            return dateTime.ToString(Globals.LongDateTimeFormat);
        }
        public static string NullableToString(this object obj)
        {
            return obj == null ? null : obj.ToString();
        }
        public static void Set(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }

    public class EnumListItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public IconAttribute IconObject { get; set; }
    }
    public class IconAttribute : Attribute
    {
        public string Path { get; set; }
        public string Title { get; set; }
    }
}
