﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;

namespace MetalPortal.Core
{
    public class Mailer
    {
        public const string MailHost = "smtp.sendgrid.net";
        public const string MailPort = "25";
        public const string MailUser = "apikey";
        public const string MailPassword = "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";
        public const string MailReturnAddress = "info@mpsignal.ru";


        public static void SendEmail(string address, string subject, string body, List<string> files = null, bool sync = false)
        {
            MailMessage mailMessage = new MailMessage(Mailer.MailReturnAddress, address, subject, body) {
                IsBodyHtml = true
            };
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    Attachment attachment = new Attachment(file);
                    mailMessage.Attachments.Add(attachment);
                }
            }

            SmtpClient client = new SmtpClient(Mailer.MailHost, int.Parse(Mailer.MailPort))
            {
                Credentials = new NetworkCredential(Mailer.MailUser, Mailer.MailPassword),
                EnableSsl = false
            };
            client.SendCompleted += (s, e) => {
                client.Dispose();
                mailMessage.Dispose();
            };

            if (!sync)
                client.SendAsync(mailMessage, null);
            else
                client.Send(mailMessage);
        }
    }
}
