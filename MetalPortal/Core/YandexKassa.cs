﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using MetalPortal.Core.Models;
using Newtonsoft.Json;

namespace MetalPortal.Core
{
    public class YandexKassa
    {
        const string BaseUrl = "https://payment.yandex.net/api/v3/payments";

        public async static Task<object> CreatePayment(Globals globals, Payment payment, string returnUrl = "")
        {
            object data = new
            {
                amount = new
                {
                    value = payment.Amount,
                    currency = "RUB"
                },
                capture = true,
                confirmation = new
                {
                    type = "redirect",
                    return_url = "http://mpsignal.ru/payment/succeeded?paymentId=" + payment.Id.ToString() + "&returnUrl=" + returnUrl
                },
                description = "Оплата услуги \"" + payment.Service.Name + "\"",
                metadata = new
                {
                    paymentId = payment.Id
                }
            };

            YandexKassaRequest request = new YandexKassaRequest(globals, YandexKassa.BaseUrl, data);
            return JsonConvert.DeserializeObject(await request.Send());
        }
    }

    public class YandexKassaRequest
    {
        protected Globals globals { get; set; }
        protected HttpClient client { get; set; }
        protected string url { get; set; }
        protected object data { get; set; }

        public YandexKassaRequest(Globals globals, string url, object data = null)
        {
            this.client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes(globals.configuration["YandexKassa:ShopId"] + ":" + globals.configuration["YandexKassa:SecretKey"])));
            client.DefaultRequestHeaders.Add("Idempotence-Key", Guid.NewGuid().ToString());
            this.url = url;
            this.data = data;
        }

        async public Task<string> Send()
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (this.data != null)
                response = await this.client.PostAsync(this.url, new StringContent(this.data.toJSON(), Encoding.UTF8, "application/json"));
            else
                response = await this.client.GetAsync(this.url);

            return await response.Content.ReadAsStringAsync();
        }
    }
}
