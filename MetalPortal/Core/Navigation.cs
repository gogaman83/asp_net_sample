﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetalPortal.Core.Models;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace MetalPortal.Core
{
    //public static class NavigationFactory
    //{
    //    public static Navigation<T> Create(IServiceProvider serviceProvider, string root)
    //    {
    //        return Assembly.GetAssembly(typeof(Navigation)).GetTypes().Where(t => t.IsSubclassOf(typeof(Navigation<T>)))
    //            .Select(t => (Navigation<T>)ActivatorUtilities.CreateInstance(serviceProvider, t)).FirstOrDefault(x => x.Root == root);
    //    }
    //}

    public class Navigation<T> where T : EntityProduct
    {
        protected Globals globals { get; set; }
        protected List<NavigationSuperGroup<T>> superGroups { get; set; }

        public Navigation(Globals globals)
        {
            this.globals = globals;
            this.superGroups = new List<NavigationSuperGroup<T>> {
                new CatalogueSuperGroupCatalogueId<T>(globals, this.Root, this.MinGroupSize),
                new CatalogueSuperGroupSize<T>(globals, this.Root, this.MinGroupSize),
                new CatalogueSuperGroupSteelGrade<T>(globals, this.Root, this.MinGroupSize)
            };
        }
        public string Name => typeof(T) == typeof(Offer) ? "Предложения поставщиков" : "Заявки покупателей";
        public string Root => typeof(T) == typeof(Offer) ? "offers" : "orders";
        public bool ShowActiveSuperGroup => typeof(T) == typeof(OrderItem);
        public virtual int MinGroupSize => typeof(T) == typeof(Offer) ? 3 : int.MaxValue;
        public NavigationResult<T> GetSuperGroupsByUrlParts(List<string> urlParts)
        {
            int[,] variants = new int[2,3] {{0,1,2},{0,2,1}};

            for (var i = 0; i < 2; i++)
            {
                var partsCombination = this.superGroups.ToDictionary(x => x.Field, x => string.Empty);

                for (var j = 0; j < 3; j++)
                {
                    if (urlParts[variants[i, j]].NotNullOrEmpty())
                        partsCombination[partsCombination.ElementAt(j).Key] = urlParts[variants[i,j]];
                }
                var result = this.GetSuperGroupsByUrlPartsCombination(partsCombination);
                if (result != null)
                    return result;
            }

            return null;
        }
        protected NavigationResult<T> GetSuperGroupsByUrlPartsCombination(Dictionary<string, string> urlCombination)
        {
            var initialQuery = this.globals.db.Set<T>().AsQueryable().Where(x => x.Size != "").Where(x => x.SteelGrade != "");

            var result = new NavigationResult<T> {
                Query = initialQuery,
                SuperGroups = new List<NavigationSuperGroup<T>>(this.superGroups)
            };

            //Фильтруем по выбранным группам
            foreach (var superGroup in result.SuperGroups)
            {
                superGroup.Groups.Clear();
                //Инициируем актитвную группу
                if (urlCombination[superGroup.Field].NotNullOrEmpty())
                {
                    superGroup.ActiveGroup = superGroup.GetActiveGroupFromUrl(urlCombination[superGroup.Field]);
                    if (superGroup.ActiveGroup == null)
                        return null;
                    else
                    {
                        //Фильтруем по выбранным группам
                        result.Query = superGroup.AddWhere(result.Query, superGroup.ActiveGroup);
                        superGroup.ActiveGroup.Url = superGroup.GetUrl(superGroup.ActiveGroup, result.SuperGroups);
                    }
                }
            }

            //Пустой урл
            if (result.SuperGroups.All(x => x.ActiveGroup == null))
            {
                var firstSuperGroup = result.SuperGroups.First(x => x.GetType() == typeof(CatalogueSuperGroupCatalogueId<T>));
                //firstSuperGroup.Groups = this.globals.db.Catalogue.OrderBy(x => x.Name).ToList().Select(x => new NavigationGroup(x.Id, x.Name)).ToList();
                firstSuperGroup.Groups = this.globals.db.Set<T>().Select(x => x.Catalogue).Distinct().OrderBy(x => x.Name).ToList().Select(x => new NavigationGroup(x.Id, x.Name, x.Image)).ToList();
                firstSuperGroup.Groups.ForEach(x => x.Url = this.Root + "/" + firstSuperGroup.GetUrl(x));
            }
            else
            {
                //Формируем группы
                foreach (var superGroup in result.SuperGroups.Where(x => x.ActiveGroup == null || this.ShowActiveSuperGroup))
                {
                    var g = superGroup.GroupQuery(this.ShowActiveSuperGroup ? initialQuery : result.Query).ToList();
                    superGroup.Groups = g.Where(x => (x.Count >= superGroup.MinGroupSize || (superGroup.GetType() == typeof(CatalogueSuperGroupCatalogueId<T>) && this.ShowActiveSuperGroup)) && x.Name.NotNullOrEmpty()).OrderBy(x => x.Name).ToList();
                    superGroup.Groups.ForEach(x => x.Url = superGroup.GetUrl(x, result.SuperGroups));
                }
                //Формируем элементы без групп
                foreach (var superGroup in result.SuperGroups.Where(x => x.ActiveGroup == null))
                    superGroup.Groups.ForEach(x => result.Query = superGroup.AddNotWhere(result.Query, x));
            }

            return result;
        }
    }

    public class NavigationSuperGroup<T> where T : EntityProduct
    {
        public string Field { get; set; }
        public string Title { get; set; }
        public NavigationGroup ActiveGroup { get; set; }
        public Type Type { get; set; }
        public List<NavigationGroup> Groups { get; set; }
        public int MinGroupSize { get; set; }
        protected Globals globals { get; set; }
        protected string root { get; set; }

        public NavigationSuperGroup(Globals globals)
        {
            this.globals = globals;
            this.Groups = new List<NavigationGroup>();
        }
        public NavigationSuperGroup(Globals globals, string field, string title, Type type, string root, int minGroupSize) : this(globals)
        {
            this.Field = field;
            this.Title = title;
            this.Type = type;
            this.root = root;
            this.MinGroupSize = minGroupSize;
        }
        public virtual NavigationGroup GetActiveGroupFromUrl(string url)
        {
            return null;
        }
        public string GetUrl(NavigationGroup group, List<NavigationSuperGroup<T>> superGroups = null)
        {
            if (superGroups != null)
            {
                var urls = superGroups.ToDictionary(x => x.Field, x => (x.ActiveGroup != null ? x.GetUrl(x.ActiveGroup) : string.Empty));
                urls[this.Field] = this.GetUrl(group);
                return this.root + "/" + string.Join("/", urls.Where(x => x.Value.NotNullOrEmpty()).Select(x => x.Value));
            }
            else
            {
                return group.Name.toURL();
            }
        }
        public IQueryable<T> AddWhere(IQueryable<T> query, NavigationGroup group)
        {
            return query.Where(this.GetExpression(group));
        }
        public IQueryable<T> AddNotWhere(IQueryable<T> query, NavigationGroup group)
        {
            var expression = this.GetExpression(group);
            var notExpression = Expression.Lambda<Func<T, bool>>(Expression.Not(expression.Body),expression.Parameters);
            return query.Where(notExpression);
        }
        public virtual Expression<Func<T, bool>> GetExpression(NavigationGroup group)
        {
            return null;
        }
        public virtual IQueryable<NavigationGroup> GroupQuery(IQueryable<T> query)
        {
            return null;
        }
    }
    public class CatalogueSuperGroupCatalogueId<T> : NavigationSuperGroup<T> where T : EntityProduct
    {
        public CatalogueSuperGroupCatalogueId(Globals globals, string root, int minGroupSize) : base(globals, "CatalogueId", "Тип товара", typeof(int), root, minGroupSize)
        {
        }
        public override NavigationGroup GetActiveGroupFromUrl(string url)
        {
            var catalogue = this.globals.db.Catalogue.ToList().FirstOrDefault(x => x.Url == url.ToLower());
            return new NavigationGroup(catalogue.Id, catalogue.Name);
        }
        public override Expression<Func<T, bool>> GetExpression(NavigationGroup group)
        {
            return x => x.CatalogueId == (int)group.Id;
        }
        public override IQueryable<NavigationGroup> GroupQuery(IQueryable<T> query)
        {
            return query.GroupBy(x => new { x.Catalogue.Name }).Select(g => new NavigationGroup(g.Key.Name, g.Count()));
        }
    }
    public class CatalogueSuperGroupSize<T> : NavigationSuperGroup<T> where T : EntityProduct
    {
        public CatalogueSuperGroupSize(Globals globals, string root, int minGroupSize) : base(globals, "Size", "Размер", typeof(string), root, minGroupSize)
        {
        }
        public override IQueryable<NavigationGroup> GroupQuery(IQueryable<T> query)
        {
            return query.GroupBy(x => new { x.Size }).Select(g => new NavigationGroup(g.Key.Size, g.Count()));
        }
        public override NavigationGroup GetActiveGroupFromUrl(string url)
        {
            var name = this.globals.db.Set<T>().Select(x => x.Size).Distinct().FirstOrDefault(x => x.toURL() == url);
            return name.NotNullOrEmpty() ? new NavigationGroup(name) : null;
        }
        public override Expression<Func<T, bool>> GetExpression(NavigationGroup group)
        {
            return x => x.Size == group.Name;
        }
    }
    public class CatalogueSuperGroupSteelGrade<T> : NavigationSuperGroup<T> where T : EntityProduct
    {
        public CatalogueSuperGroupSteelGrade(Globals globals, string root, int minGroupSize) : base(globals, "SteelGrade", "Марка стали", typeof(string), root, minGroupSize)
        {
        }
        public override IQueryable<NavigationGroup> GroupQuery(IQueryable<T> query)
        {
            return query.GroupBy(x => new { x.SteelGrade }).Select(g => new NavigationGroup(g.Key.SteelGrade, g.Count()));
        }
        public override Expression<Func<T, bool>> GetExpression(NavigationGroup group)
        {
            return x => x.SteelGrade == group.Name;
        }
        public override NavigationGroup GetActiveGroupFromUrl(string url)
        {
            var name = this.globals.db.Set<T>().Select(x => x.SteelGrade).Distinct().FirstOrDefault(x => x.toURL() == url);
            return new NavigationGroup(name);
        }
    }

    public class NavigationGroup
    {
        public object Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public int Count { get; set; }

        public NavigationGroup()
        {

        }

        public NavigationGroup(string name) : this()
        {
            this.Name = name;
            this.Id = name;
        }

        public NavigationGroup(object id, string name) : this(name)
        {
            this.Id = id;
        }

        public NavigationGroup(string name, int count) : this(name, name)
        {
            this.Count = count;
        }


        public NavigationGroup(object id, string name, string image) : this(id, name)
        {
            this.Image = image;
        }
    }

    public class NavigationResult<T> where T : EntityProduct
    {
        public List<NavigationSuperGroup<T>> SuperGroups { get; set; }
        public IQueryable<T> Query { get; set; }

        public NavigationResult()
        {
            this.SuperGroups = new List<NavigationSuperGroup<T>>();
        }
    }

}
