﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace MetalPortal.Core
{
    public class UploadsManager
    {
        protected string uploadsRoot;
        protected ApplicationContext db;

        public UploadsManager(ApplicationContext context)
        {
            this.uploadsRoot = "wwwroot/uploads";
            this.db = context;
        }

        public string RootPath
        {
            get
            {
                return Directory.GetCurrentDirectory() + "/wwwroot";
            }
        }

        public string GetUploadsPath(string folder = null, bool create = false)
        {
            var path = Directory.GetCurrentDirectory() + "/" + this.uploadsRoot;

            if (folder != null)
                path = path + "/" + folder;

            if (create)
                Directory.CreateDirectory(path);

            return path;
        }

        public string GetUploadsLink(string folder, string file)
        {
            return $"/uploads/{folder}/{file}";
        }

        public async Task<Upload> UploadUrl(string url, string folder)
        {
            string relativeFolderPath = "/" + this.uploadsRoot + "/" + folder;
            string fullFolderPath = Directory.GetCurrentDirectory() + relativeFolderPath;

            Directory.CreateDirectory(fullFolderPath);

            string fileName = Guid.NewGuid().ToString() + "_" + url.Split("/").Last();
            string fileRelativePath = relativeFolderPath + "/" + fileName;
            string fileFullPath = fullFolderPath + "/" + fileName;

            using (var client = new HttpClient())
            {

                using (var result = await client.GetAsync(url))
                {
                    if (result.IsSuccessStatusCode)
                    {
                        using (var fileStream = new FileStream(fileFullPath, FileMode.Create))
                        {
                            fileStream.Write(await result.Content.ReadAsByteArrayAsync());
                        }
                    }

                }
            }

            Upload upload = new Upload(url, fileRelativePath);

            this.db.Add<Upload>(upload);
            this.db.SaveChanges();

            return upload;
        }

        public Upload UploadFile(IFormFile file, string folder)
        {
            string relativeFolderPath = "/" + this.uploadsRoot + "/" + folder;
            string fullFolderPath = Directory.GetCurrentDirectory() + relativeFolderPath;

            Directory.CreateDirectory(fullFolderPath);

            string fileName = Guid.NewGuid().ToString() + "_" + file.FileName;
            string fileRelativePath = relativeFolderPath + "/" + fileName;
            string fileFullPath = fullFolderPath + "/" + fileName;

            using (var fileStream = new FileStream(fileFullPath, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }

            Upload upload = new Upload(file.FileName, fileRelativePath);

            this.db.Add<Upload>(upload);
            this.db.SaveChanges();

            return upload;
        }
    }
}
