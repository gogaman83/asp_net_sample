﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using Microsoft.EntityFrameworkCore;

namespace MetalPortal.Controllers
{
    public class NavigationController : BaseController
    {
        protected IServiceProvider serviceProvider;
        public NavigationController(Globals globals, IServiceProvider serviceProvider) : base(globals)
        {
            this.serviceProvider = serviceProvider;
        }

        [HttpGet]
        public ActionResult GetList(string root, string category1 = "", string category2 = "", string category3 = "", string category4 = "")
        {
            if (root == "offers")
                return this.GetList<Offer>(category1, category2, category3, category4);
            else
                return this.GetList<OrderItem>(category1, category2, category3, category4);
        }

        protected ActionResult GetList<T>(string category1 = "", string category2 = "", string category3 = "", string category4 = "") where T : EntityProduct
        {
            var navigation = new Navigation<T>(this.globals);
            var pageModel  = new NavigationViewModel<T>(navigation.Name, navigation.Root);

            pageModel.NavigationResult = navigation.GetSuperGroupsByUrlParts(new List<string> { category1, category2, category3, category4 });
            pageModel.BreadCrumbs.AddRange(pageModel.NavigationResult.SuperGroups.Where(x => x.ActiveGroup != null).Select(x => new BreadCrumb(x.ActiveGroup.Name, x.ActiveGroup.Url)));
            pageModel.Title = string.Join("/", pageModel.NavigationResult.SuperGroups.Where(x => x.ActiveGroup != null).Select(x => x.ActiveGroup.Name));
            pageModel.Title = navigation.Name + (pageModel.Title.NotNullOrEmpty() ? "/" : "") + pageModel.Title;
            pageModel.H1 = pageModel.Title.Replace("/", " / ");

            if (category1.NotNullOrEmpty() || navigation.ShowActiveSuperGroup)
            {
                pageModel.NavigationResult.Query = pageModel.NavigationResult.Query.OrderBy(x => x.Catalogue.Name).ThenBy(x => x.Size).ThenBy(x => x.SteelGrade).Include(x => x.Catalogue);
                if (typeof(T) == typeof(OrderItem))
                {
                    pageModel.Items = pageModel.NavigationResult.Query.ToList();
                    var ids = pageModel.Items.Select(x => x.Id);
                    pageModel.Orders = globals.db.Orders.Where(x => x.OrderItems.Any(i => ids.Contains(i.Id))).Include(x => x.User).Include(x => x.OrderItems).Include(x => x.Raisings).ToList().OrderBy(x => x.SortingOrder).ToList();
                }
                else if (typeof(T) == typeof(Offer))
                {
                    pageModel.Offers = ((IQueryable<Offer>)pageModel.NavigationResult.Query).Include(x => x.Vendor).ToList();
                }
            }

            return View("GetList" + (typeof(T) == typeof(Offer) ? "Offers" : "Orders"),pageModel);
        }
    }
}
