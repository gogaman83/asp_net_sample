﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;

namespace MetalPortal.Controllers
{
    public class BaseController : Controller
    {
        protected Globals globals;

        public BaseController(Globals globals)
        {
            this.globals = globals;
        }

        protected JsonResult SuccessJson(string message, object data = null)
        {
            return this.Json(new { success = true, message = message, data = data });
        }
        protected JsonResult SuccessJson(object obj = null)
        {
            return this.SuccessJson("", obj);
        }

        protected JsonResult ErrorJson(string message, object data = null)
        {
            return this.Json(new { success = false, message, data, errors = ModelState.ToList().Select(x => new {
                    Key = x.Key.toCamelCase(),
                    x.Value.Errors,
                    x.Value.ValidationState
                })
            });
        }
        protected JsonResult ErrorJson(object obj = null)
        {
            return this.ErrorJson("", obj);
        }

        protected JsonResult ForbiddenJson()
        {
            return this.ErrorJson("У вас недостаточно прав");
        }

        protected JsonResult DataSourceJSON<T>(IQueryable<T> countQuery, IQueryable<T> listQuery)
        {
            return this.Json(new
            {
                data = listQuery.ToList(),
                total = countQuery.Count(),
                error = ""
            });
        }
    }
}
