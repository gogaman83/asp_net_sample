﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetalPortal.Controllers
{
    public class DataController : BaseController
    {
        public DataController(Globals globals) : base(globals)
        {

        }

        [HttpGet]
        public JsonResult Search(string search, int take = 10)
        {

            List<SearchResultItem> resultItems = new List<SearchResultItem>();

            if (search.NotNullOrEmpty())
            {
                var searchResult = (new Search(this.globals, search, take)).Execute();

                resultItems.AddRange(searchResult.Vendors.Select(x => new SearchResultItem
                {
                    Type = SearchResultItemTypes.Vendor,
                    ShortText = x.Name,
                    Url = "vendor/" + x.Id.ToString()
                }).OrderBy(x => x.ShortText));
                resultItems.AddRange(searchResult.CatalogueItems.Select(x => new SearchResultItem
                {
                    Type = SearchResultItemTypes.Catalogue,
                    ShortText = x.Name,
                    Url = "offers/" + x.Url
                }).OrderBy(x => x.ShortText));
                resultItems.AddRange(searchResult.Offers.Select(x => new SearchResultItem
                {
                    Type = SearchResultItemTypes.Offer,
                    ShortText = x.Catalogue.Name + " " + x.Size + " " + x.SteelGrade,
                    Url = "offer/" + x.Id.ToString()
                }).OrderBy(x => x.ShortText));
                resultItems.AddRange(searchResult.OrderItems.Select(x => new SearchResultItem
                {
                    Type = SearchResultItemTypes.Order,
                    ShortText = x.Catalogue.Name + " " + x.Size + " " + x.SteelGrade,
                    Url = "order/" + x.OrderId.ToString()
                }).OrderBy(x => x.ShortText));
                resultItems.AddRange(searchResult.Adverts.Select(x => new SearchResultItem
                {
                    Type = SearchResultItemTypes.Advert,
                    ShortText = x.CatalogueItem.Name + " / " + x.Title,
                    Url = "advert/" + x.Id.ToString()
                }).OrderBy(x => x.ShortText));
            }

            return this.Json(resultItems);
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetEnum(string enumName)
        {
            var type = Type.GetType(enumName);

            if (type.IsEnum)
                return this.SuccessJson(type.EnumToList());
            else
                return this.ErrorJson();
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetServiceActivations(bool? isActive = null)
        {
            var query = this.globals.db.PaidServicesActivations.Where(x => x.UserId == this.globals.user.Id).Include(x => x.LimitSubscriptionOrders).OrderByDescending(x => x.CreateDate);
            var data = query.Include(x => x.User).Include(x => x.Service).Include(x => x.Payment).ToList();
            data.ForEach(x => x.IsActiveCalculated = x.IsActive(this.globals));

            if (isActive.HasValue)
                data = data.Where(x => x.IsActiveCalculated == isActive.Value).ToList();

            return this.Json(new
            {
                data,
                total = data.Count
            });
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetPayments()
        {
            var query = this.globals.db.Payments.Where(x => x.UserId == this.globals.user.Id).Where(x => x.Type == PaymentTypes.Bank || x.Status == PaymentStatuses.Succeeded).OrderByDescending(x => x.CreateDate);

            return this.Json(new
            {
                data = query.Include(x => x.User.UserAccounting).Include(x => x.Service).ToList(),
                total = query.Count()
            });
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetUnpaidAccounts(string filter = null)
        {
            if (this.globals.user.IsAdmin)
            {
                var query = this.globals.db.Payments.Where(x => x.Type == PaymentTypes.Bank && x.Status == PaymentStatuses.Waiting);

                if (filter.NotNullOrEmpty())
                    query = query.Where(x => x.User.UserAccounting != null && (x.Id.ToString().Contains(filter.ToLower().Trim()) || x.User.UserAccounting.PayerName.ToLower().Contains(filter.ToLower().Trim())));

                return this.Json(new {
                    data = query.Include(x => x.User.UserAccounting).Include(x => x.Service).ToList(),
                    total = query.Count()
                });
            }
            else
                return this.ForbiddenJson();
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetUserAccounting()
        {
            var userAccounting = this.globals.user.UserAccounting;

            if (userAccounting == null)
            {
                userAccounting = new UserAccounting
                {
                    Email  = this.globals.user.Email,
                    UserId = this.globals.user.Id
                };
            }
            return this.Json(userAccounting);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetPaymentTypes()
        {
            return this.Json(typeof(PaymentTypes).EnumToList());
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetPaidServices()
        {
            return this.Json(globals.db.PaidServices.Where(x => !x.Deleted).OrderBy(x => x.Price));
        }

        [HttpGet]
        public JsonResult GetVendors(int take, int skip, string search)
        {
            if (search.IsNullOrEmpty())
                search = "";

            var query = globals.db.Users
                .Where(x => x.UserRole == UserRoles.Vendor)
                .Where(x => x.Name.ToLower().Contains(search.ToLower()))
                .OrderBy(x => x.Status)
                .ThenBy(x => x.CreateDate);

            return this.DataSourceJSON(query, query.Skip(skip).Take(take));
        }

        [HttpGet]
        public JsonResult GetSteelGrades(int take, string search, string filtersJSON)
        {
            if (search.IsNullOrEmpty())
                search = "";

            var query = globals.db.Offers.Where(x => x.SteelGrade.ToLower().Contains(search.ToLower()));

            if (filtersJSON.NotNullOrEmpty())
            {
                try
                {
                    var filterObject = JsonConvert.DeserializeObject<Offer>(filtersJSON);
                    if (filterObject.CatalogueId > 0)
                        query = query.Where(x => x.CatalogueId == filterObject.CatalogueId);
                }
                catch (Exception exc) { }
            }

            return this.Json(query.Select(x => x.SteelGrade).Distinct().OrderBy(x => x).ToList());
        }

        //adverts
        [HttpGet]
        public ActionResult GetAdvert(int advertId)
        {
            var advert = globals.db.Adverts.Where(x => x.Id == advertId).Include(x => x.User).Include(x => x.CatalogueItem).Include(x => x.Uploads).FirstOrDefault();
            advert.Uploads = advert.Uploads.Where(x => !x.Deleted).ToList();

            return this.Json(advert);
        }

        //orders
        [HttpGet]
        [Authorize]
        public JsonResult GetOrders(int take, int skip)
        {
            var query = globals.db.Orders.OrderByDescending(x => x.Id).AsQueryable();

            if (!globals.user.IsAdmin)
                query = query.Where(x => x.UserId == this.globals.user.Id);

            return this.DataSourceJSON(query, query.Include(x => x.Raisings).Skip(skip).Take(take));
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetOrderFeedbacks(int orderId)
        {
            var order = globals.db.Orders.Find(orderId);

            if (order.CanChange(globals))
            {
                var query = globals.db.OrderFeedbacks.Where(x => x.OrderId == orderId).OrderByDescending(x => x.CreateDate).AsQueryable();

                return this.DataSourceJSON(query, query.Include(x => x.User));
            }
            else
                return this.Forbid();
        }

        [HttpGet]
        public ActionResult GetOrderItems(int take, int skip, int orderId)
        {
            var order = globals.db.Orders.Find(orderId);

            if (order.CanChange(globals))
            {
                var query = globals.db.OrderItems.Where(x => x.OrderId == orderId).OrderBy(x => x.Id).AsQueryable();

                return this.DataSourceJSON(query, query.Skip(skip).Take(take).Include(x => x.Order));
            }
            else
                return this.Forbid();
        }

        [HttpGet]
        public JsonResult GetCatalogueItems()
        {
            return this.Json(globals.db.Catalogue.OrderBy(x => x.Name).ToList());
        }

        [HttpGet]
        public JsonResult GetOffers(Dictionary<string, string> filters, int take, int skip, bool forEdit = false, int? userId = null, string searchText = "")
        {
            var query = globals.db.Offers.AsQueryable();

            if (forEdit && this.globals.user != null)
                query = query.Where(x => x.VendorId == this.globals.user.Id);
            else if (searchText.NotNullOrEmpty())
            {
                var ids = (new Search(this.globals, searchText, int.MaxValue)).Execute().OffersIds;
                query = query.Where(x => ids.Contains(x.Id));
            }
            else if (userId.HasValue)
                query = query.Where(x => x.VendorId == userId.Value);

            var filtersSet = new FilterSetProducts(globals);
            foreach (var filter in filtersSet.Filters)
                filter.Value = filters.ContainsKey(filter.PropertyName) ? filters[filter.PropertyName] : null;

            query = filtersSet.Apply(query);

            return this.DataSourceJSON(query, query.Skip(skip).Take(take).Include(x => x.Catalogue).Include(x => x.Vendor));
        }

        [HttpGet]
        public JsonResult GetOffer(int offerId)
        {
            return this.Json(this.globals.db.Offers.Where(x => x.Id == offerId).Include(x => x.Catalogue).Include(x => x.Vendor).FirstOrDefault());
        }

        [HttpGet]
        public JsonResult GetVendorReviews(int vendorId)
        {
            return this.Json(this.globals.db.VendorReviews.Where(x => x.VendorId == vendorId && x.Publish).Include(x => x.Author).Include(x => x.Rating).OrderByDescending(x => x.Id).Select(x => new
            {
                x.Id,
                x.Message,
                x.Author.Name,
                x.CreateDate,
                x.Rating.Rating
            }));
        }
    }

    public class SearchResultItem
    {
        public string ShortText { get; set; }
        public string Url { get; set; }
        public SearchResultItemTypes Type { get; set; }
        public List<string> Words { get; set; }
        public string TypeName
        {
            get
            {
                return this.Type.DisplayName();
            }
        }
        public SearchResultItem()
        {
            this.Words = new List<string>();
        }
    }

    public enum SearchResultItemTypes
    {
        [Display(Name = "Категория")]
        Catalogue = 0,
        [Display(Name = "Поставщик")]
        Vendor = 1,
        [Display(Name = "Предложение")]
        Offer = 2,
        [Display(Name = "Заявка")]
        Order = 3,
        [Display(Name = "Объявление")]
        Advert = 4
    }
}


