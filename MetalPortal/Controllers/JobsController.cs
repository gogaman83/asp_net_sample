﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OfficeOpenXml;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Spire.Xls;

namespace MetalPortal.Controllers
{
    public class jobsController : BaseController
    {

        public jobsController(Globals globals) : base(globals)
        {
        }

        public async Task<ActionResult> login()
        {
            var newUser = new User
            {
                UserName = "+79106674536",
                PhoneNumber = "+79106674536"
            };

            var createResult = await globals.userManager.CreateAsync(newUser, "1234");

            var user = this.globals.db.Users.Where(x => x.PhoneNumber == "+7 (777) 777-77-77").FirstOrDefault();
            var user1 = this.globals.db.Users.Where(x => x.Email == "derburt@mail.ru").FirstOrDefault();
            var result = await this.globals.signInManager.PasswordSignInAsync("+79106674536", "1234", true, false);

            user.PhoneNumberConfirmed = true;

            this.globals.db.SaveChanges();

            return Content("ok");
        }

        public async Task<ActionResult> excel()
        {
            try
            {
                var dir = Directory.GetCurrentDirectory();

                var sourceFileName = this.globals.uploadsManager.RootPath + "/account.xlsx";
                var targetFileName = this.globals.uploadsManager.GetUploadsPath("accounts", true) + "/Счет №999.pdf";


                using (Workbook workbook = new Workbook())
                {
                    workbook.LoadFromFile(sourceFileName);

                    var sheet = workbook.ActiveSheet;

                    sheet.Range["B13"].Text = "Счет за красивую жизнь";
                    sheet.Range["Z6"].Text = DateTime.Now.ToLongTimeString();
                    sheet.Range["AB22"].Value = "22";

                    workbook.SaveToFile(targetFileName, Spire.Xls.FileFormat.PDF);
                    Mailer.SendEmail("gogaman@mail.ru", "счет", "Ваш счет", new List<string> { targetFileName });
                }
            }
            catch(Exception exception)
            {
                Log.Add(this.globals, exception);
            }

            //using (var package = new ExcelPackage(new FileStream(dir + "/misc/bill.xlsx", FileMode.Open)))
            //{
            //    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
            //    worksheet.Cells["Z6"].Value = DateTime.Now.ToLongTimeString();
            //    var fileStream = new FileStream(dir + "/misc/Счет №44.xlsx", FileMode.Create);
            //    var memoryStream = new MemoryStream();
            //    package.SaveAs(fileStream);
            //    fileStream.Close();
            //    Mailer.SendEmail("gogaman@mail.ru", "счет", "Ваш счет", new List<string> { dir + "/misc/Счет №44.xlsx" });
            //}

            return Content("ok");
        }

        public async Task<ActionResult> activatePayment()
        {
            var payment = this.globals.db.Payments.Where(x => x.Id == 14).Include(x => x.Service).FirstOrDefault();

            if (payment != null)
            {

                payment.Status = PaymentStatuses.Succeeded;
                payment.ExternalId = "test";
                payment.SetStatusDate = DateTime.Now;

                var userService = this.globals.db.PaidServicesActivations.Where(x => x.PaymentId == payment.Id).FirstOrDefault();

                if (userService == null)
                {
                    userService = new PaidServiceActivation
                    {
                        UserId = this.globals.user.Id,
                        PaymentId = payment.Id,
                        ServiceId = payment.ServiceId
                    };

                    this.globals.db.PaidServicesActivations.Add(userService);
                }

                await this.globals.db.SaveChangesAsync();
            }

            return Content("ok");
        }

        public ActionResult fillServices()
        {
            this.globals.db.PeriodSubscriptions.Add(new PeriodSubscription
            {
                Name = "Подписка на месяц",
                Price = 99,
                PriceOld = 120,
                Period = new SubscriptionPeriod
                {
                     Months = 1
                }
            });

            this.globals.db.PeriodSubscriptions.Add(new PeriodSubscription
            {
                Name = "Подписка на 3 месяца",
                Price = 99,
                PriceOld = 120,
                Period = new SubscriptionPeriod
                {
                    Months = 3
                }
            });

            this.globals.db.PeriodSubscriptions.Add(new PeriodSubscription
            {
                Name = "Подписка на год",
                Price = 99,
                PriceOld = 120,
                Period = new SubscriptionPeriod
                {
                    Years = 1
                }
            });

            this.globals.db.LimitSubscriptions.Add(new LimitSubscription
            {
                Name = "Подписка на 5 объявлений",
                Price = 49,
                PriceOld = 70,
                Period = new SubscriptionPeriod
                {
                    Months = 1
                },
                Limit = 5
            });

            this.globals.db.SaveChanges();

            return Content("ok");
        }

        public ActionResult succeeded()
        {
            return Content("ok");
        }

        public async Task<ActionResult> notification()
        {
            using (var reader = new StreamReader(this.globals.contextAccessor.HttpContext.Request.Body))
            {
                var body = reader.ReadToEnd();
                var notificationTypeObject = new { @event = "", @object = new { id = "", status = "", metadata = new { paymentId = 0 } } };
                var data = JsonConvert.DeserializeAnonymousType(body, notificationTypeObject);
                this.globals.db.Log.Add(new Log(body));
                this.globals.db.Log.Add(new Log(data));
                await this.globals.db.SaveChangesAsync();
            }
            return Content("ok");
        }

        public async Task<ActionResult> check(string paymentId)
        {

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("677728:test_19uwxdDB19y6jB_tNjks4vGeXpAaJrP6eo0XBp5FlMM")));

            var response = await client.GetAsync("https://payment.yandex.net/api/v3/payments/" + paymentId);
            string resultContent = await response.Content.ReadAsStringAsync();

            return Content(resultContent);
        }

        public async Task<ActionResult> pay()
        {

            var client = new HttpClient();

            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("677728:test_19uwxdDB19y6jB_tNjks4vGeXpAaJrP6eo0XBp5FlMM")));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes(this.globals.configuration["YandexKassa:ShopId"] + ":" + this.globals.configuration["YandexKassa:SecretKey"])));
            client.DefaultRequestHeaders.Add("Idempotence-Key", Guid.NewGuid().ToString());

            object jsonObject = new
            {
                amount = new
                {
                    value = "1",
                    currency = "RUB"
                },
                capture = true,
                confirmation = new
                {
                    type = "redirect",
                    return_url = "http://mpsignal.ru/payment/succeeded"
                },
                description = "Заказ №1",
                metadata = new {
                    paymentId = 759
                }
            };

            var content = new StringContent(jsonObject.toJSON(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync("https://payment.yandex.net/api/v3/payments", content);
            string resultContent = await response.Content.ReadAsStringAsync();

            return Content(resultContent);
        }

        public async Task<ActionResult> LoadPricelists()
        {
            foreach(var vendor in this.globals.db.Users.Where(x => x.UserRole == UserRoles.Vendor && x.PricelistUrl != null).ToList())
            {
                var lastPricelist = this.globals.db.Pricelists.Where(x => x.VendorId == vendor.Id).OrderBy(x => x.CreateDate).LastOrDefault();

                if (lastPricelist == null || (DateTime.Now - lastPricelist.CreateDate) > new TimeSpan(0,0,0,1))
                    await (new Pricelist(this.globals, vendor)).Load(vendor.PricelistUrl);
            }

            return Content("ok");
        }
    }
}
