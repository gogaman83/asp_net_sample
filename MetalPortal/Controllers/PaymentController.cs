﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;


namespace MetalPortal.Controllers
{
    public class PaymentController : BaseController
    {
        public PaymentController(Globals globals) : base(globals)
        {

        }

        public async Task<ActionResult> Notify()
        {
            using (var reader = new StreamReader(this.globals.contextAccessor.HttpContext.Request.Body))
            {
                var body = reader.ReadToEnd();

                var data = JsonConvert.DeserializeAnonymousType(body, new
                {
                    @event = "",
                    @object = new
                    {
                        id = "",
                        status = "",
                        metadata = new
                        {
                            paymentId = 0
                        }
                    }
                });

                Log.Add(this.globals, data);

                var payment = this.globals.db.Payments.Where(x => x.Id == data.@object.metadata.paymentId).Include(x => x.Service).FirstOrDefault();

                if (payment != null && data.@object.status == "succeeded")
                    payment.SetSuccess(this.globals, data.@object.id);
            }
            return Content("ok");
        }

        public async Task<JsonResult> CreateOnline(int serviceId, string returnUrl = "")
        {

            var service = this.globals.db.PaidServices.Find(serviceId);

            var payment = new Payment
            {
                Service = service,
                User = this.globals.user,
                Amount = service.Price,
                Type = PaymentTypes.Online,
                Status = PaymentStatuses.Waiting
            };

            this.globals.db.Payments.Add(payment);
            this.globals.db.SaveChanges();

            try
            {
                return this.Json(await YandexKassa.CreatePayment(this.globals, payment, returnUrl));
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public JsonResult CreateBank(int serviceId, string returnUrl = "")
        {

            var service = this.globals.db.PaidServices.Find(serviceId);

            var payment = new Payment
            {
                Service = service,
                User = this.globals.user,
                Amount = service.Price,
                Type = PaymentTypes.Bank,
                Status = PaymentStatuses.Created
            };

            this.globals.db.Payments.Add(payment);
            this.globals.db.SaveChanges();

            var file = payment.SendAccount(this.globals);

            payment.Status = PaymentStatuses.Waiting;
            this.globals.db.SaveChanges();

            var userAccount = this.globals.db.UserAccountings.FirstOrDefault(x => x.UserId == payment.UserId);

            return this.SuccessJson(new
            {
                file,
                email = userAccount.Email
            });
        }
    }
}
