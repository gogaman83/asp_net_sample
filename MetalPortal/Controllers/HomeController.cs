﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace MetalPortal.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(Globals globals) : base(globals)
        {

        }

        public IActionResult Index()
        {
            var user = globals.user;
            return View(new PageViewModel("index"));
        }


        public ActionResult Search(string text)
        {
            return View(new PageViewModel("search") {
                Object = text
            });
        }

        [HttpGet]
        public ActionResult OnPaymentSuccess(int paymentId, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new PageViewModel("payment_success") {
                Object = this.globals.db.Payments.Where(x => x.Id == paymentId).Include(x => x.Service).FirstOrDefault()
            });
        }

        [HttpGet]
        public ActionResult Subscribe(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new PageViewModel("subscribe"));
        }

        public ActionResult Products()
        {
            return View(new PageViewModel("products"));
        }

        public ActionResult ServiceAgreement()
        {
            return View(new PageViewModel("serviceAgreement"));
        }

        public ActionResult Contacts()
        {
            return View(new PageViewModel("contacts"));
        }

        [HttpGet]
        public ActionResult Vendor(int id)
        {
            var vendor = this.globals.db.Users.Where(x => x.Id == id).Include(x => x.Ratings).Include(x => x.Reviews).FirstOrDefault();
            return View(new PageViewModel("vendor") {
                 Object = vendor
            });
        }

        [HttpGet]
        public ActionResult OrderNew()
        {
            return View(new OrderNewViewModel());
        }

        [HttpGet]
        [Authorize]
        public ActionResult CabinetOrders()
        {
            return View(new PageViewModel("cabinetOrders"));
        }

        [HttpGet]
        public ActionResult Order(int id)
        {
            return View(new PageViewModel("order")
            {
                Object = globals.db.Orders.Where(x => x.Id == id).Include(x => x.User).Include(x => x.OrderItems).ThenInclude(x => x.Catalogue).First()
            });
        }

        [HttpGet]
        public ActionResult Offer(int id)
        {
            return View(new PageViewModel("offer")
            {
                Object = globals.db.Offers.Where(x => x.Id == id).Include(x => x.Vendor).Include(x => x.Catalogue).First()
            });
        }

        [HttpGet]
        public ActionResult Adverts()
        {
            return View(new PageViewModel("adverts"));
        }

        [HttpGet]
        public ActionResult AdvertsAdd()
        {
            return View(new PageViewModel("adverts_add"));
        }

        [HttpGet]
        public ActionResult Advert(int id)
        {
            return View(new PageViewModel("advert")
            {
                Object = id
            });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
