﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Core;

namespace MetalPortal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class advertController : BaseController
    {
        public advertController(Globals globals) : base(globals)
        {

        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("{id}")]
        public string Get(int id)
        {
            return $"get {id}";
        }

        [HttpPost("{id}/raise")]
        public string Raise(int id)
        {
            return $"raise {id}";
        }

        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
