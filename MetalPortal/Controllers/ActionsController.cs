﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.IO;

namespace MetalPortal.Controllers
{
    public class ActionsController : BaseController
    {
        public ActionsController(Globals globals) : base(globals)
        {

        }

        //Payment
        [Authorize]
        [HttpPost]
        public ActionResult SetAccountPaid(int paymentId, string externalId = null)
        {
            if (this.globals.user.IsAdmin)
            {
                var payment = this.globals.db.Payments.Find(paymentId);
                payment.SetSuccess(this.globals, externalId);
                return this.SuccessJson();
            }
            else
                return this.ForbiddenJson();
        }

        //Advert
        [HttpPost]
        public ActionResult CreateOrUpdateAdvert(Advert advert, IFormFile file)
        {
            var dbAdvert = advert.Id != 0 ? globals.db.Adverts.Find(advert.Id) : null;

            ModelState.Clear();

            if (advert.Title.IsNullOrEmpty())
                ModelState.AddModelError("Title", "Заполните заголовок");
            if (advert.Name.IsNullOrEmpty())
                ModelState.AddModelError("Name", "Заполните имя");
            if (advert.Text.IsNullOrEmpty())
                ModelState.AddModelError("Text", "Заполните текст");
            if (advert.Phone.IsNullOrEmpty())
                ModelState.AddModelError("Phone", "Заполните номер телефона");
            if (advert.Email.IsNullOrEmpty())
                ModelState.AddModelError("Email", "Заполните email");
            if (advert.CatalogueItemId == 0)
                ModelState.AddModelError("CatalogueItemId", "Заполните категорию");

            if (ModelState.IsValid)
            {
                if (advert.Id == 0)
                {
                    if (this.globals.user != null)
                        advert.UserId = this.globals.user.Id;

                    this.globals.db.Add<Advert>(advert);
                    this.globals.db.SaveChanges();

                    if (file != null)
                        advert.UploadFiles(this.globals, new List<IFormFile> { file });

                    return this.SuccessJson("Объявление успешно добавлено");
                }
                else if (dbAdvert.CanChange(globals))
                {
                    dbAdvert.Title = advert.Title;
                    dbAdvert.Text = advert.Text;
                    dbAdvert.Email = advert.Email;
                    dbAdvert.Phone = advert.Phone;
                    dbAdvert.Organization = advert.Organization;
                    dbAdvert.Type = advert.Type;
                    dbAdvert.CatalogueItemId = advert.CatalogueItemId;

                    globals.db.SaveChanges();

                    if (file != null)
                        advert.UploadFiles(this.globals, new List<IFormFile> { file });

                    return this.SuccessJson("Объявление успешно изменено");
                }
                else
                    return this.Forbid();
            }
            else
                return this.ErrorJson();
        }

        [Authorize]
        [HttpPost]
        public ActionResult RaiseAdvert([FromBody] Advert advert)
        {
            var dbAdvert = globals.db.Adverts.Where(x => x.Id == advert.Id).Include(x => x.Raisings).FirstOrDefault();

            if (dbAdvert.CanChange(globals) && dbAdvert.CanBeRaised)
            {
                globals.db.AdvertRaisings.Add(new AdvertRaising()
                {
                    AdvertId = dbAdvert.Id
                });

                globals.db.SaveChanges();

                return this.SuccessJson("Объявление " + advert.Id.ToString() + " успешно поднято");
            }
            else
                return this.ErrorJson("Объявление " + advert.Id.ToString() + " не может быть поднято");
        }

        [HttpPost]
        [Authorize]
        public ActionResult RemoveAdvert([FromBody]Advert advert)
        {
            var dbAdvert = globals.db.Adverts.Find(advert.Id);

            if (dbAdvert != null)
            {
                if (dbAdvert.CanChange(globals))
                {
                    dbAdvert.Delete();
                    globals.db.SaveChanges();
                    return this.SuccessJson("Объявление " + advert.Id.ToString() + " успешно удалено");
                }
                else
                    return this.Forbid();
            }
            else
                return this.ErrorJson("Заявка не найдена");
        }

        //Order
        [HttpPost]
        public JsonResult CreateOrder([FromBody] Order order)
        {
            if (this.globals.user != null)
                order.UserId = this.globals.user.Id;

            this.globals.db.Add<Order>(order);
            this.globals.db.SaveChanges();
            return this.SuccessJson();
        }

        [HttpPost]
        public JsonResult CreateOrderFromOffer(int offerId, Order order, double quantity)
        {
            if (order.Name.IsNullOrEmpty())
                ModelState.AddModelError("Name", "Укажите имя");

            if (quantity <= 0)
                ModelState.AddModelError("quantity", "Укажите количество");

            if (order.Email.IsNullOrEmpty())
                ModelState.AddModelError("Email", "Укажите электронную почту");
            else if (!Utils.IsValidEmail(order.Email))
                ModelState.AddModelError("Email", "Неверная электронная почта");

            if (order.Phone.IsNullOrEmpty())
                ModelState.AddModelError("Phone", "Укажите телефон");
            else if (!Utils.IdValidPhone(order.Phone))
                ModelState.AddModelError("Phone", "Неверный телефон");

            if (ModelState.IsValid)
            {
                if (this.globals.user != null)
                    order.UserId = this.globals.user.Id;

                var offer = this.globals.db.Offers.Find(offerId);

                order.OrderItems.Add(new OrderItem
                {
                    Size = offer.Size,
                    CatalogueId = offer.CatalogueId,
                    MetalType = offer.MetalType,
                    SteelGrade = offer.SteelGrade,
                    Quantity = quantity,
                    Article = offer.Article,
                    OfferId = offerId
                });

                this.globals.db.Add<Order>(order);
                this.globals.db.SaveChanges();
                return this.SuccessJson(order);
            }
            else
                return this.ErrorJson(this.ModelState);

        }
        [HttpPost]
        [Authorize]
        public ActionResult UpdateOrder(Order order)
        {
            var dbOrder = globals.db.Orders.Find(order.Id);

            if (dbOrder.CanChange(globals))
            {

                dbOrder.Phone = order.Phone;
                dbOrder.Comment = order.Comment;
                dbOrder.Name = order.Name;

                globals.db.SaveChanges();

                return this.Json(dbOrder);
            }
            else
                return this.Forbid();
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateOrderStatus([FromBody] Order order)
        {
            var dbOrder = globals.db.Orders.Find(order.Id);
            if (dbOrder.CanChange(globals))
            {
                dbOrder.Status = order.Status;
                globals.db.SaveChanges();
                return this.SuccessJson();
            }
            else
                return this.Forbid();
        }

        [Authorize]
        [HttpPost]
        public ActionResult RaiseOrder([FromBody] Order order)
        {
            var dbOrder = globals.db.Orders.Where(x => x.Id == order.Id).Include(x => x.Raisings).FirstOrDefault();

            if (dbOrder.CanChange(globals) && dbOrder.CanBeRaised)
            {
                globals.db.OrderRaisings.Add(new OrderRaising()
                {
                    OrderId = dbOrder.Id
                });

                globals.db.SaveChanges();

                return this.SuccessJson();
            }
            else
                return this.Forbid();
        }

        [HttpPost]
        [Authorize]
        public ActionResult RemoveOrder(Order order)
        {
            var dbOrder = globals.db.Orders.Find(order.Id);

            if (dbOrder != null)
            {
                if (dbOrder.CanChange(globals))
                {
                    globals.db.Orders.Remove(dbOrder);
                    globals.db.SaveChanges();
                    return this.SuccessJson();
                }
                else
                    return this.Forbid();
            }
            else
                return this.ErrorJson("Заявка не найдена");
        }

        //OrderFeedback
        [Authorize]
        [HttpPost]
        public ActionResult AddOrderFeedback([FromBody] OrderFeedback feedback)
        {
            if (this.globals.user.IsVendor)
            {
                if (feedback.Text.IsNullOrEmpty())
                    ModelState.AddModelError("Text", "Введите сообщение");

                if (ModelState.IsValid)
                {
                    feedback.UserId = globals.user.Id;

                    globals.db.OrderFeedbacks.Add(feedback);

                    globals.db.SaveChanges();

                    return this.SuccessJson("Ваш ответ принят");
                }
                else
                    return this.ErrorJson();
            }
            else
                return this.Forbid();
        }

        //OrderItem
        [HttpPost]
        [Authorize]
        public ActionResult CreateOrderItem(OrderItem orderItem)
        {
            var order = globals.db.Orders.Find(orderItem.OrderId);

            if (order.CanChange(globals))
            {
                OrderItem newOrderItem = new OrderItem
                {
                    CatalogueId = orderItem.CatalogueId,
                    SteelGrade = orderItem.SteelGrade,
                    Size = orderItem.Size,
                    Quantity = orderItem.Quantity,
                    OrderId = orderItem.OrderId
                };
                globals.db.OrderItems.Add(newOrderItem);
                globals.db.SaveChanges();

                return this.Json(newOrderItem);
            }
            else
                return this.Forbid();
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateOrderItem(OrderItem orderItem)
        {
            var dbOrderItem = globals.db.OrderItems.Where(x => x.Id == orderItem.Id).Include(x => x.Order).FirstOrDefault();

            if (dbOrderItem.Order.CanChange(globals))
            {

                dbOrderItem.CatalogueId = orderItem.CatalogueId;
                dbOrderItem.SteelGrade = orderItem.SteelGrade;
                dbOrderItem.Quantity = orderItem.Quantity;
                dbOrderItem.Size = orderItem.Size;

                globals.db.SaveChanges();

                return this.Json(dbOrderItem);
            }
            else
                return this.Forbid();
        }

        [HttpPost]
        [Authorize]
        public ActionResult RemoveOrderItem(OrderItem orderItem)
        {
            var dbOrderItem = globals.db.OrderItems.Where(x => x.Id == orderItem.Id).Include(x => x.Order).FirstOrDefault();

            if (dbOrderItem.Order.CanChange(globals))
            {
                globals.db.OrderItems.Remove(dbOrderItem);
                globals.db.SaveChanges();
                return this.SuccessJson();
            }
            else
                return this.Forbid();
        }

        //Offer
        [Authorize]
        [HttpPost]
        public JsonResult CreateOffer([FromForm] Offer offer)
        {
            Offer dbOffer = new Offer()
            {
                VendorId = this.globals.user.Id,
                CatalogueId = offer.CatalogueId,
                Size = offer.Size,
                SteelGrade = offer.SteelGrade,
                MetalType = offer.MetalType,
                Quantity = offer.Quantity,
                Article = offer.Article,
                Comment = offer.Comment
            };
            this.globals.db.Add<Offer>(dbOffer);
            this.globals.db.SaveChanges();

            return this.Json(new
            {
                data = this.globals.db.Offers.Where(x => x.Id == dbOffer.Id).Include(x => x.Catalogue).FirstOrDefault()
                //total = 1,
                //error = ""
            });
        }

        [Authorize]
        [HttpPost]
        public JsonResult UpdateOffer([FromForm] Offer offer)
        {
            var dbOffer = this.globals.db.Offers.Where(x => x.VendorId == this.globals.user.Id && x.Id == offer.Id).FirstOrDefault();

            if (dbOffer != null)
            {
                dbOffer.CatalogueId = offer.CatalogueId;
                dbOffer.Size = offer.Size;
                dbOffer.SteelGrade = offer.SteelGrade;
                dbOffer.MetalType = offer.MetalType;
                dbOffer.Quantity = offer.Quantity;
                dbOffer.Article = offer.Article;
                dbOffer.Price = offer.Price;
                dbOffer.Comment = offer.Comment;

                this.globals.db.SaveChanges();

                return this.Json(this.globals.db.Offers.Where(x => x.Id == dbOffer.Id).Include(x => x.Catalogue).FirstOrDefault());
            }
            else
                return this.ErrorJson("Заказ не найден");
        }

        [Authorize]
        [HttpPost]
        public JsonResult RemoveOffer([FromForm] Offer offer)
        {
            var dbOffer = this.globals.db.Offers.Where(x => x.VendorId == this.globals.user.Id && x.Id == offer.Id).FirstOrDefault();

            if (dbOffer != null)
            {
                this.globals.db.Remove(dbOffer);
                this.globals.db.SaveChanges();
                return this.SuccessJson();
            }
            else
                return this.ErrorJson("Заказ не найден");
        }

        //User
        [Authorize]
        [HttpPost]
        public ActionResult UpdateUser([FromBody] User model)
        {
            if (model.Id == globals.user.Id)
            {
                var dbUser = this.globals.db.Users.Find(model.Id);

                if (model.Name.IsNullOrEmpty())
                    ModelState.AddModelError("Name", "Укажите имя");

                if (model.PhoneNumber.IsNullOrEmpty())
                    ModelState.AddModelError("PhoneNumber", "Укажите телефон");
                else if (!Utils.IdValidPhone(model.PhoneNumber))
                    ModelState.AddModelError("PhoneNumber", "Неверный телефон");


                if (dbUser.UserRole == UserRoles.Vendor)
                {
                    if (model.Organization.IsNullOrEmpty())
                        ModelState.AddModelError("Organization", "Укажите организацию");

                    if (model.Url.NotNullOrEmpty())
                    {
                        if (!Utils.IsValidUrl(Utils.FormatUrl(model.Url)))
                            ModelState.AddModelError("Url", "Неверный адрес сайта");
                    }

                    if (model.PricelistUrl.NotNullOrEmpty())
                    {
                        if (!Utils.IsValidUrl(Utils.FormatUrl(model.PricelistUrl)))
                            ModelState.AddModelError("PricelistUrl", "Неверная ссылка на прайс-лист");
                    }


                    
                }

                if (ModelState.IsValid)
                {
                    dbUser.PhoneNumber = model.PhoneNumber;
                    dbUser.Name = model.Name;
                    dbUser.Organization = model.Organization;
                    dbUser.Url = Utils.FormatUrl(model.Url.Trim());
                    dbUser.PricelistUrl = Utils.FormatUrl(model.PricelistUrl.Trim());
                    this.globals.db.SaveChanges();
                    return this.SuccessJson();
                }
                else
                    return this.ErrorJson();
            }
            else
                return this.Forbid();
        }

        [Authorize]
        [HttpPost]
        public JsonResult UpdateVendor([FromBody] User vendor)
        {
            var dbVendor = globals.db.Users.Find(vendor.Id);
            dbVendor.Status = vendor.Status;
            globals.db.SaveChanges();
            return this.SuccessJson();
        }

        [Authorize]
        [HttpPost]
        public JsonResult UpdateUserAccounting(UserAccounting userAccounting)
        {
            if (userAccounting.Email.IsNullOrEmpty())
                ModelState.AddModelError("Email", "Укажите email");

            if (userAccounting.PaymentAccount.IsNullOrEmpty())
                ModelState.AddModelError("PaymentAccount", "Укажите расчетный счет");

            if (userAccounting.PayerName.IsNullOrEmpty())
                ModelState.AddModelError("PayerName", "Укажите имя плательщика");

            if (userAccounting.ITN.IsNullOrEmpty())
                ModelState.AddModelError("ITN", "Укажите ИНН");

            if (userAccounting.BankId.IsNullOrEmpty())
                ModelState.AddModelError("BankId", "Укажите БИК");

            if (userAccounting.BankName.IsNullOrEmpty())
                ModelState.AddModelError("BankName", "Укажите наименование банка");

            if (ModelState.IsValid)
            {
                if (userAccounting.Id == 0)
                    this.globals.db.UserAccountings.Add(userAccounting);
                else
                {
                    var entity = this.globals.db.UserAccountings.Find(userAccounting.Id);
                    this.globals.db.Entry(entity).CurrentValues.SetValues(userAccounting);
                }
                this.globals.db.SaveChanges();
                return this.SuccessJson(userAccounting);
            }
            else
                return this.ErrorJson(ModelState);
        }

        //Prices
        [Authorize]
        [HttpPost]
        public JsonResult LoadPricelist(IFormFile pricelistfile)
        {
            Pricelist pricelist = new Pricelist(this.globals, globals.user);

            if (pricelist.Load(pricelistfile))
                return this.SuccessJson("Прайс-лист успешно загружен");
            else
                return this.ErrorJson("Ошибка загрузки прайс-листа", pricelist.Errors.Select(x => x.ToString()));
        }

        [Authorize]
        [HttpPost]
        public JsonResult LoadPricelistColumns([FromBody]Dictionary<string,string> columns)
        {
            Pricelist pricelist = new Pricelist(this.globals, globals.user);

            Dictionary<string, List<string>> columnsRows = columns.ToDictionary(x => x.Key, x => x.Value.Split('\n').Select(s => s.Trim()).Where(s => s.NotNullOrEmpty()).ToList());

            if (columnsRows.Max(x => x.Value.Count) == 0)
                return this.ErrorJson("Пустой прайс-лист");
            else if (pricelist.Load(columnsRows))
                return this.SuccessJson("Прайс-лист успешно загружен");
            else
                return this.ErrorJson("Ошибка загрузки прайс-листа", pricelist.Errors.OrderBy(x => x.Cell).ThenBy(x => x.Row).ToList());
        }

        [HttpPost]
        [Authorize]
        public JsonResult AddVendorFeedback(int vendorId, int? rating = null, string message = null, string name = null)
        {
            int? authorId = this.globals.user != null ? (int?)this.globals.user.Id : null;
            VendorRating vendorRating = null;

            var existingRating = this.globals.db.VendorRatings.FirstOrDefault(x => x.AuthorId == authorId && x.VendorId == vendorId);
            var existingReview = this.globals.db.VendorReviews.FirstOrDefault(x => x.AuthorId == authorId && x.VendorId == vendorId);

            if (rating.HasValue && existingRating == null)
            {
                vendorRating = new VendorRating
                {
                    AuthorId = authorId,
                    Rating = rating.Value,
                    VendorId = vendorId
                };
                this.globals.db.VendorRatings.Add(vendorRating);
            }

            if (message.NotNullOrEmpty() && existingReview == null)
            {
                var review = new VendorReview
                {
                    AuthorId = authorId,
                    AuthorName = name,
                    Message = message,
                    VendorId = vendorId,
                    Rating = vendorRating,
                    Publish = true
                };
                this.globals.db.VendorReviews.Add(review);
            }

            this.globals.db.SaveChanges();

            return this.SuccessJson("Спасибо за Ваш отзыв!");
        }
    }
}
