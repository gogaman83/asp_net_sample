﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace MetalPortal.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(Globals globals) : base(globals)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await globals.signInManager.SignOutAsync();
            return LocalRedirect("/");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnURL = "")
        {
            return View(new LoginViewModel
            {
                ReturnURL = returnURL
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (model.Email.IsNullOrEmpty())
                ModelState.AddModelError("Email", "Укажите email");

            if (model.Password.IsNullOrEmpty())
                ModelState.AddModelError("Password", "Укажите пароль");

            if (ModelState.IsValid)
            {
                var result = await globals.signInManager.PasswordSignInAsync(model.Email, model.Password, true, false);
                if (result.Succeeded)
                {
                    if (model.ReturnURL.NotNullOrEmpty())
                        return LocalRedirect(model.ReturnURL);
                    else
                        return LocalRedirect("/");
                }
                else
                {
                    ModelState.AddModelError("Email", "Неверный email/пароль");
                    return View(model);
                }
            }
            else
                return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> LoginAJAX(string login, string password)
        {
            if (login.IsNullOrEmpty())
                ModelState.AddModelError("login", "Укажите логин");

            if (password.IsNullOrEmpty())
                ModelState.AddModelError("password", "Укажите пароль");

            if (ModelState.IsValid)
            {
                var user = this.globals.db.Users.FirstOrDefault(x => x.Email.ToLower() == login.Trim().ToLower());
                if (user == null)
                    user = this.globals.db.Users.FirstOrDefault(x => x.PhoneNumber.Contains(login.Trim()));

                if (user != null)
                {
                    var result = await globals.signInManager.PasswordSignInAsync(user.UserName, password, true, false);
                    if (result.Succeeded)
                        return this.SuccessJson();
                    else
                    {
                        ModelState.AddModelError("login", "Неверный логин/пароль");
                        return this.ErrorJson(ModelState);
                    }
                }
                else
                {
                    ModelState.AddModelError("login", "Неверный логин/пароль");
                    return this.ErrorJson(ModelState);
                }
            }
            else
                return this.ErrorJson(ModelState);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAJAX(RegisterViewModel model)
        {
            if (model.UserName.IsNullOrEmpty())
                ModelState.AddModelError("UserName", "Укажите имя");

            if (model.Email.IsNullOrEmpty())
                ModelState.AddModelError("Email", "Укажите электронную почту");
            else if (!Utils.IsValidEmail(model.Email))
                ModelState.AddModelError("Email", "Неверная электронная почта");

            if (model.Phone.IsNullOrEmpty())
                ModelState.AddModelError("Phone", "Укажите телефон");
            else if (!Utils.IdValidPhone(model.Phone))
                ModelState.AddModelError("Phone", "Неверный телефон");
            
            if (model.Password.IsNullOrEmpty())
                ModelState.AddModelError("Password", "Укажите пароль");
            else if (model.ConfirmPassword != model.Password)
                ModelState.AddModelError("ConfirmPassword", "Подтверждение не совпадает с паролем");

            if (model.UserRole == UserRoles.Vendor)
            {
                if (model.Organization.IsNullOrEmpty())
                    ModelState.AddModelError("Organization", "Укажите организацию");

                if (model.OrganizationUrl.NotNullOrEmpty())
                {
                    if (!Utils.IsValidUrl(Utils.FormatUrl(model.OrganizationUrl)))
                        ModelState.AddModelError("OrganizationUrl", "Неверный адрес сайта");
                }

                if (model.PricelistUrl.NotNullOrEmpty())
                {
                    if (!Utils.IsValidUrl(Utils.FormatUrl(model.PricelistUrl)))
                        ModelState.AddModelError("PricelistUrl", "Неверная ссылка на прайс-лист");
                }
            }

            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.Email.Trim(),
                    Email = model.Email.Trim(),
                    Name = model.UserName.Trim(),
                    UserRole = model.UserRole,
                    PhoneNumber = Utils.ClearPhone(model.Phone.Trim()),
                    Organization = model.Organization.NotNullOrEmpty() ? model.Organization.Trim() : null
                };

                var emailUser = this.globals.db.Users.FirstOrDefault(x => x.Email == user.Email);
                var phoneUser = this.globals.db.Users.FirstOrDefault(x => x.PhoneNumber == user.PhoneNumber);

                if (emailUser != null)
                    ModelState.AddModelError("Email", "Пользователь с таким email уже зарегистрирован");
                if (phoneUser != null)
                    ModelState.AddModelError("Email", "Пользователь с таким номером телефона уже зарегистрирован");

                if (ModelState.IsValid)
                {
                    if (!model.PhoneVerification || model.ResendPhoneVerification)
                    {
                        var phoneApprovement = new PhoneApprovement
                        {
                            Phone = user.PhoneNumber,
                            Code = Globals.GenerateRandomString(4, "123456789"),
                            CreateTime = DateTime.UtcNow
                        };
                        this.globals.contextAccessor.HttpContext.Session.Set("phoneApprovement", phoneApprovement);
                        model.PhoneVerification = true;
                        SMSSender.Send(phoneApprovement.Phone, Globals.ApplicationDomain + " Код регистрации: " + phoneApprovement.Code);
                        return this.SuccessJson(model);
                    }
                    else
                    {
                        var phoneApprovement = this.globals.contextAccessor.HttpContext.Session.Get<PhoneApprovement>("phoneApprovement");
                        if (phoneApprovement == null || phoneApprovement.IsExpired)
                        {
                            ModelState.AddModelError("VerificationCode", "Срок действия код истек");
                            return this.ErrorJson(ModelState);
                        }
                        else if (phoneApprovement.Code != model.VerificationCode || phoneApprovement.Phone != user.PhoneNumber)
                        {
                            ModelState.AddModelError("VerificationCode", "Неверный проверочный код");
                            return this.ErrorJson(ModelState);
                        }
                    }

                    if (model.OrganizationUrl.NotNullOrEmpty())
                        user.Url = Utils.FormatUrl(model.OrganizationUrl.Trim());

                    if (model.PricelistUrl.NotNullOrEmpty())
                        user.PricelistUrl = Utils.FormatUrl(model.PricelistUrl.Trim());

                    var createResult = await globals.userManager.CreateAsync(user, model.Password);

                    if (createResult.Succeeded)
                    {
                        await globals.signInManager.SignInAsync(user, true);
                        Mailer.SendEmail(model.Email, "Регистрация на портале " + Globals.ApplicationDomain, "Здравствуйте, " + model.UserName + "!<br/>Вы успешно зарегистированы на портале " + Globals.ApplicationAnchor + "<br/>Ваш пароль: " + model.Password);
                        return this.SuccessJson();
                    }
                    else if (createResult.Errors.Any(x => x.Code == "DuplicateUserName"))
                    {
                        ModelState.AddModelError("Email", "Такой email уже зарегистрирован");
                        return this.ErrorJson(ModelState);
                    }
                    else
                    {
                        ModelState.AddModelError("Email", "Неизвестная ошибка");
                        return this.ErrorJson(ModelState);
                    }
                }
                else
                    return this.ErrorJson(ModelState);
            }
            else
                return this.ErrorJson(ModelState);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register(string returnURL = "")
        {
            return View(new RegisterViewModel {
                ReturnURL = (returnURL.NotNullOrEmpty() ? returnURL : "/login")
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (model.UserName.IsNullOrEmpty())
                ModelState.AddModelError("Name", "Укажите имя");

            if (model.Email.IsNullOrEmpty())
                ModelState.AddModelError("Email", "Укажите электронную почту");
            else if (!Utils.IsValidEmail(model.Email))
                ModelState.AddModelError("Email", "Неверная электронная почта");

            if (model.Phone.IsNullOrEmpty())
                ModelState.AddModelError("Phone", "Укажите телефон");
            else if (!Utils.IdValidPhone(model.Phone))
                ModelState.AddModelError("Phone", "Неверный телефон");


            if (model.Password.IsNullOrEmpty())
                ModelState.AddModelError("Password", "Укажите пароль");
            else if (model.ConfirmPassword != model.Password)
                ModelState.AddModelError("ConfirmPassword", "Подтверждение не совпадает с паролем");

            if (model.UserRole == UserRoles.Vendor)
            {
                if (model.Organization.IsNullOrEmpty())
                    ModelState.AddModelError("Organization", "Укажите организацию");

                if (model.OrganizationUrl.NotNullOrEmpty())
                {
                    if (!Utils.IsValidUrl(Utils.FormatUrl(model.OrganizationUrl)))
                        ModelState.AddModelError("OrganizationUrl", "Неверный адрес сайта");
                }

                if (model.PricelistUrl.NotNullOrEmpty())
                {
                    if (!Utils.IsValidUrl(Utils.FormatUrl(model.PricelistUrl)))
                        ModelState.AddModelError("PricelistUrl", "Неверная ссылка на прайс-лист");
                }
            }

            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.Email.Trim(),
                    Email = model.Email.Trim(),
                    Name = model.UserName.Trim(),
                    UserRole = model.UserRole,
                    PhoneNumber = Utils.ClearPhone(model.Phone.Trim()),
                    Organization = model.Organization.NotNullOrEmpty() ? model.Organization.Trim() : null
                };

                if (model.OrganizationUrl.NotNullOrEmpty())
                    user.Url = Utils.FormatUrl(model.OrganizationUrl.Trim());

                if (model.PricelistUrl.NotNullOrEmpty())
                    user.PricelistUrl = Utils.FormatUrl(model.PricelistUrl.Trim());

                var createResult = await globals.userManager.CreateAsync(user, model.Password);

                if (createResult.Succeeded)
                {
                    //await globals.signInManager.SignInAsync(user, true);
                    Mailer.SendEmail(model.Email, "Регистрация на портале " + Globals.ApplicationDomain, "Здравствуйте, " + model.UserName + "!<br/>Вы успешно зарегистированы на портале " + Globals.ApplicationAnchor + "<br/>Ваш пароль: " + model.Password);
                    model.Success = true;
                    return View(model);
                }
                else if (createResult.Errors.Any(x => x.Code == "DuplicateUserName"))
                {
                    ModelState.AddModelError("Email", "Такой email уже зарегистрирован");
                    return View(model);
                }
                else
                {
                    ModelState.AddModelError("Email", "Неизвестная ошибка");
                    return View(model);
                }
            }
            else
                return View(model);
        }

        public string GeneratePassword(int length = 8)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
                res.Append(valid[rnd.Next(valid.Length)]);
            return res.ToString();
        }
    }

    public class PhoneApprovement
    {
        public string Phone { get; set; }
        public string Code { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsExpired
        {
            get
            {
                return (DateTime.UtcNow - this.CreateTime).TotalSeconds > 10;
            }
        }
    }
}

