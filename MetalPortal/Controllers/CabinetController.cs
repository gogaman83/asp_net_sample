﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace MetalPortal.Controllers
{
    [Authorize]
    public class CabinetController : BaseController
    {
        public CabinetController(Globals globals): base(globals)
        {

        }
        public ActionResult Index()
        {
            return View(new PageViewModel("cabinet") {
                Object = this.globals.user
            });
        }
        public ActionResult Payments()
        {
            return View(new PageViewModel("cabinet/payments"));
        }
        public ActionResult Subscriptions()
        {
            return View(new PageViewModel("cabinet/subscriptions"));
        }
        public ActionResult UnpaidAccounts()
        {
            return View(new PageViewModel("cabinet/unpaid_accounts"));
        }
        public ActionResult Orders()
        {
            return View(new PageViewModel("cabinet/orders"));
        }
        public ActionResult Adverts()
        {
            return View(new PageViewModel("cabinet/adverts"));
        }
        public ActionResult Advert(int id)
        {
            return View(new PageViewModel("cabinet/advert")
            {
                Object = id
            });
        }
        public ActionResult Offers()
        {
            return View(new PageViewModel("cabinet/offers"));
        }
        public ActionResult Vendors()
        {
            return View(new PageViewModel("cabinet/vendors"));
        }
        public ActionResult LoadPricelist()
        {
            return View(new PageViewModel("cabinet/load_pricelist"));
        }
    }
}
