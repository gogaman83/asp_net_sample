"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var react_redux_1 = require("react-redux");
var store_1 = require("./store");
require("./css/app.scss");
require("./css/themes/default.scss");
var addSubscription_1 = require("./components/addSubscription");
//App
var App = /** @class */ (function (_super) {
    __extends(App, _super);
    function App() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    App.prototype.render = function () {
        var components = {
            AddSubscription: addSubscription_1.default
        };
        var ComponentName = components[this.props.component];
        return (React.createElement("div", { className: "m_" + this.props.component },
            React.createElement(ComponentName, null)));
    };
    return App;
}(React.Component));
//connect
var AppRedux = react_redux_1.connect(function (store) {
    return {
        store: store
    };
}, function (dispatch) { return ({
    go: function (name) { return dispatch({
        type: 'GO',
        payload: name
    }); }
}); })(App);
window.app = {
    init: function (element, component, options) {
        var app = ReactDOM.render(React.createElement(react_redux_1.Provider, { store: store_1.default },
            React.createElement(AppRedux, { component: component, options: options })), element);
        return { app: app, store: store_1.default };
    }
};
//# sourceMappingURL=app.js.map