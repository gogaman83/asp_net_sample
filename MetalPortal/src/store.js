"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var store = redux_1.createStore(function (state, action) {
    switch (action.type) {
        case 'GO':
            return __assign(__assign({}, state), { name: action.payload });
        default:
            return state;
    }
}, {
    name: 'One'
});
exports.default = store;
//# sourceMappingURL=store.js.map