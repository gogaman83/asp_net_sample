﻿import * as React from 'react';

export default class Loader extends React.Component<any, any> {

    render() {

        const style = {
            width: '100%',
            height: '100%',
            top: '0px',
            left: '0px'
        }

        return (
            <div>
                <div className="k-loading-mask" style={style}>
                    <span className="k-loading-text">Loading...</span>
                    <div className="k-loading-image"></div>
                    <div className="k-loading-color"></div>
                </div>
            </div>
        );
    }
}