﻿import * as React from 'react';

export default class Form extends React.Component<any, any> {

    constructor(props) {

        super(props);

        this.state = {
            errors: []
        }

    }

    public processErrors(response) {
        if (!response.success) {
            this.setState({
                errors: response.errors
            })
        }
        else {
            this.setState({
                errors: []
            })
        }
    }

    render() {


        const childrenWithProps = React.Children.map(this.props.children, child =>
        {
            const childElement = child as React.ReactElement;
            if (childElement) {
                const error = this.state.errors.length ? this.state.errors.find(x => x.key == childElement.props.name) : false;
                return React.cloneElement(childElement, { error: error ? error.errors.map(x => x.errorMessage).join(", ") : "" });
            }
            else
                return childElement;
        });

        return (

            <form className="m_standart_form">
                <fieldset>
                    <legend></legend>
                    <ol>
                        {childrenWithProps}
                    </ol>
                </fieldset>
            </form>
        );
    }
}