﻿import * as React from 'react';

export default class FormRow extends React.Component<any, any> {

    constructor(props) {

        super(props);

    }

    render() {

        return (
            <li>
                <label className={this.props.required ? 'required' : ''}>{this.props.title}</label>
                {this.props.children}
                {this.props.error && <span className="field-validation-error" data-valmsg-replace="true">{this.props.error}</span>}
            </li>
        );
    }
}