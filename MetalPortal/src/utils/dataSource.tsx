﻿import * as jquery from 'jquery';

interface DataSourceOptions {
    url: string,
    onDataLoaded(data: any): void
}

export default class DataSource {

    protected props: DataSourceOptions;

    constructor(options: DataSourceOptions) {
        this.props = options;
    }

    protected onDataLoaded(data) {
        if (this.props.onDataLoaded)
            this.props.onDataLoaded(data);
    }

    public load(params = {}) {
        jquery.get(this.props.url, params, response => {
            this.onDataLoaded(response.data);
        })
    }
}