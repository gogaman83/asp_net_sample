﻿import * as React from 'react';
import Sign from './sign';

export default class LoginPage extends React.Component<any, any> {

    onSuccess() {
        window.location = this.props.returnUrl ? this.props.returnUrl : '/';
    }

    render() {

        return (
            <div>

                {<Sign onSuccess={this.onSuccess.bind(this)} />}

            </div>
        );
    }
}