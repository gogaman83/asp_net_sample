﻿import * as React from 'react';
import * as jquery from 'jquery';
import Loader from '../utils/loader';
import DataSource from '../utils/dataSource';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import { LocalizationProvider } from '@progress/kendo-react-intl';
import FieldDate from '../templates/fieldDate';

export default class Payments extends React.Component<any, any> {

    protected dataSource: any;

    constructor(props) {

        super(props);

        this.state = {
            filter: {
                name: ''
            },
            data: [],
            statuses: [],
            types: [],
            loading: true
        }

        this.dataSource = new DataSource({
            url: '/Data/GetPayments',
            onDataLoaded: this.onDataLoaded.bind(this)
        });
    }

    protected loadData() {
        this.setState({
            loading: true
        });
        this.dataSource.load();
    }

    protected onDataLoaded(data) {
        this.setState({
            data: data,
            loading: false
        })
    }

    componentDidMount() {
        jquery.get('/Data/GetEnum', { enumName: 'MetalPortal.Core.Models.PaymentStatuses' }, statuses => {
            jquery.get('/Data/GetEnum', { enumName: 'MetalPortal.Core.Models.PaymentTypes' }, types => {
                this.setState({
                    statuses: statuses.data,
                    types: types.data
                })
                this.dataSource.load();
            })
        })
    }

    render() {
        return (
            <div>

                {this.state.loading && <Loader />}

                <LocalizationProvider language="ru-RU">
                    <Grid
                        data={this.state.data}
                    >
                        <GridColumn field="id" title="Номер счета" />
                        <GridColumn
                            field="createDate" title="Дата"
                            cell={props => (
                                <td>
                                    <FieldDate date={props.dataItem.createDate} />
                                </td>
                            )}
                        />
                        <GridColumn field="service.name" title="Подписка" />
                        <GridColumn field="amount" title="Сумма" />
                        <GridColumn field="type" title="Тип оплаты"
                            cell={props => (
                                <td>{this.state.types.find(x => x.id == props.dataItem.type).description}</td>
                            )}
                        />
                        <GridColumn field="status" title="Статус оплаты"
                            cell={props => (
                                <td>{this.state.statuses.find(x => x.id == props.dataItem.status).description}</td>
                            )}
                        />
                    </Grid>
                </LocalizationProvider>
            </div>
        );
    }

}