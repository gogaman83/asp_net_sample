﻿import * as React from 'react';
import * as jquery from 'jquery';
import Loader from '../utils/loader';
import DataSource from '../utils/dataSource';
import FieldDate from '../templates/fieldDate';
import FormRow from '../utils/formRow';
import Form from '../utils/form';
import InputMask from 'react-input-mask';
import NumberFormat from 'react-number-format';

export default class Sign extends React.Component<any, any> {

    protected dataSource: any;
    protected signInForm: any;
    protected signUpForm: any;

    constructor(props) {

        super(props);

        this.state = {
            loading: false,
            view: 'signIn',
            signin: {
                login: '',
                password: ''
            },
            signUp: {
                phoneVerification: false,
                resendPhoneVerification: false,
                userRole: 'Customer'
            }
        }

        this.signInForm = React.createRef();
        this.signUpForm = React.createRef();
    }

    componentDidMount() {
    }

    setView(view) {
        this.setState({
            view: view
        });
    }

    changeSignIn(e) {

        const target = e.target as HTMLInputElement;

        var signin = { ...this.state.signin };

        signin[target.name] = target.value;

        this.setState({
            signin: signin
        });
    }

    changeSignUp(e) {

        const target = e.target as HTMLInputElement;

        var signUp = { ...this.state.signUp };

        signUp[target.name] = target.value;

        this.setState({
            signUp: signUp
        });
    }

    changeSignUpPhone(e) {

        var signUp = { ...this.state.signUp };

        signUp.phone = e.value;

        this.setState({
            signUp: signUp
        });
    }

    changeSignUpVerificationCode(e) {
        var signUp = { ...this.state.signUp };

        signUp.verificationCode = e.value;

        this.setState({
            signUp: signUp
        });
    }

    trySignIn() {
        this.setState({
            loading: true
        })
        jquery.post("/Account/LoginAJAX", {
            login: this.state.signin.login,
            password: this.state.signin.password
        }, response => {
            this.setState({
                loading: false
            })
            if (response.success) 
                this.props.onSuccess();
            else
                this.signInForm.current.processErrors(response);
        });
    }

    trySignUp() {
        this.setState({
            loading: true
        })
        jquery.post("/Account/RegisterAJAX", this.state.signUp, response => {
            var signUp = { ...this.state.signUp };
            this.setState({
                loading: false
            })
            if (response.success) {
                this.signUpForm.current.processErrors(response);
                if (response.data && response.data.phoneVerification) {
                    signUp.phoneVerification = true;
                    signUp.resendPhoneVerification = false;
                }
                else
                    this.props.onSuccess();
            }
            else {
                this.signUpForm.current.processErrors(response);
                if (response.errors.find(x => x.key === 'verificationCode').errors.find(x => x.errorMessage.includes('Срок'))) {
                    signUp.resendPhoneVerification = true;
                }
                else {
                    signUp.resendPhoneVerification = false;
                }
            }
            this.setState({
                signUp: signUp
            });
        });
    }

    render() {

        const isVendor = (this.state.signUp.userRole === 'Vendor');

        return (
            <div className="sign_component">

                {this.state.loading && <Loader />}

                <div className="view_toggler">
                    <span className={this.state.view === 'signIn' ? 'active' : ''} onClick={this.setView.bind(this,'signIn')}>Вход</span> / 
                    <span className={this.state.view === 'signUp' ? 'active' : ''} onClick={this.setView.bind(this, 'signUp')}>Регистрация</span>
                </div>

                {this.state.view === 'signIn' && <Form ref={this.signInForm}>
                    <FormRow name="login" title="Email или номер телефона" required={true}>
                        <input type="text" name="login" onChange={this.changeSignIn.bind(this)} placeholder="email или номер телефона" />
                    </FormRow>
                    <FormRow name="password" title="Пароль" required={true}>
                        <input type="password" name="password" onChange={this.changeSignIn.bind(this)} />
                    </FormRow>
                    <FormRow name="" title="">
                        <input type="button" value={this.props.buttonText ? this.props.buttonText : "Войти"} onClick={this.trySignIn.bind(this)} />
                    </FormRow>
                </Form>}

                {this.state.view === 'signUp' && <Form ref={this.signUpForm}>
                    <FormRow name="userRole" title="">
                        <span style={{ display: 'table-cell' }}>
                            <label style={{ display: 'inline' }}><input type="radio" name="userRole" value="Customer" checked={this.state.signUp.userRole === 'Customer'} onChange={this.changeSignUp.bind(this)} /> Покупатель</label>
                            <label style={{ display: 'inline' }}><input type="radio" name="userRole" value="Vendor" checked={this.state.signUp.userRole === 'Vendor'} onChange={this.changeSignUp.bind(this)} /> Поставщик</label>
                        </span>
                    </FormRow>
                    <FormRow name="userName" title="Ваше имя" required={true}>
                        <input type="text" name="userName" onChange={this.changeSignUp.bind(this)} />
                    </FormRow>
                    {isVendor && <FormRow name="organization" title="Организация" required={true}>
                        <input type="text" name="organization" onChange={this.changeSignUp.bind(this)} />
                    </FormRow>}
                    <FormRow name="email" title="Email" required={true}>
                        <input type="text" name="email" onChange={this.changeSignUp.bind(this)} />
                    </FormRow>
                    <FormRow name="phone" title="Телефон" required={true}>
                        <NumberFormat value={this.state.signUp.phone}  format="+7 (###) ###-##-##" type="text" name="phone" onValueChange={this.changeSignUpPhone.bind(this)} />
                    </FormRow>
                    {this.state.signUp.phoneVerification && <FormRow name="verificationCode" title="Код подтверждения" required={true}>
                        <NumberFormat style={{ width: '3.5em' }} format="####" type="text" name="verificationCode" onValueChange={this.changeSignUpVerificationCode.bind(this)} />
                        {this.state.signUp.resendPhoneVerification && <input type="button" style={{ marginLeft: '1em', fontSize: '8px' }} value="Выслать повторно" onClick={this.trySignUp.bind(this)} />}
                    </FormRow>}
                    {isVendor && <FormRow name="organizationUrl" title="Адрес сайта" required={false}>
                        <input type="text" name="organizationUrl" onChange={this.changeSignUp.bind(this)} />
                    </FormRow>}
                    {isVendor && <FormRow name="pricelistUrl" title="Ссылка на прайс лист" required={false}>
                        <input type="text" name="pricelistUrl" onChange={this.changeSignUp.bind(this)} />
                    </FormRow>}
                    <FormRow name="password" title="Пароль" required={true}>
                        <input type="password" name="password" onChange={this.changeSignUp.bind(this)} />
                    </FormRow>
                    <FormRow name="confirmPassword" title="Подтверждение пароля" required={true}>
                        <input type="password" name="confirmPassword" onChange={this.changeSignUp.bind(this)} />
                    </FormRow>
                    <FormRow name="" title="">
                        <input type="button" value={this.props.buttonText ? this.props.buttonText : "Отправить"} onClick={this.trySignUp.bind(this)} />
                    </FormRow>
                </Form>}

             </div>
        );
    }

}