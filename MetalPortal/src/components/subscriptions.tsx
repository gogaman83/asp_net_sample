﻿import * as React from 'react';
import * as jquery from 'jquery';
import Loader from '../utils/loader';
import DataSource from '../utils/dataSource';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import { LocalizationProvider } from '@progress/kendo-react-intl';
import FieldDate from '../templates/fieldDate';

class SubscriptionActivationDescription extends React.Component<any, any> {
    render() {
        const data = this.props.data;

        return (
            <div>
                {data.service.limit > 0 && <div>Осталось: {data.service.limit - data.limitSubscriptionOrders.length} объявлений</div>}
                Дата окончания: <FieldDate date={data.expireDate} />
            </div>
        );
    }
}

export default class Subscriptions extends React.Component<any, any> {

    protected dataSource: any;

    constructor(props) {

        super(props);

        this.state = {
            isActive: true,
            data: [],
            loading: true
        }

        this.dataSource = new DataSource({
            url: '/Data/GetServiceActivations',
            onDataLoaded: this.onDataLoaded.bind(this)
        });
    }

    protected loadData(isActive = null) {
        this.setState({
            loading: true
        });
        this.dataSource.load({
            isActive: isActive !== null ? isActive : this.state.isActive
        });
    }

    protected onDataLoaded(data) {
        this.setState({
            data: data,
            loading: false
        })
    }

    protected handleIsActiveChange = event => {
        const isActive = event.target.value === 'active';
        this.setState({
            isActive: isActive
        });
        this.loadData(isActive);
    }

    protected subscribe = () => {
        window.location.href = '/subscribe?returnUrl=' + window.location.href;
    }

    componentDidMount() {
        this.dataSource.load();
    }

    render() {
        return (
            <div>

                {this.state.loading && <Loader />}

                <div style={{ marginBottom: '2em' }}>
                    <input type="button" value="Оформить подписку" onClick={this.subscribe} />
                </div>

                <div style={{ marginBottom: '2em' }}>
                    <label>
                        <input type="radio" value="active" checked={this.state.isActive === true} onChange={this.handleIsActiveChange} />
                        Активные
                    </label>
                    <label style={{ marginLeft: '1em' }}>
                        <input type="radio" value="noactive" checked={this.state.isActive === false} onChange={this.handleIsActiveChange} />
                        Неактивные
                    </label>
                </div>

                <LocalizationProvider language="ru-RU">
                    <Grid
                        data={this.state.data}
                    >
                        <GridColumn field="service.name" title="Подписка" />
                        <GridColumn field="amount" title="Дата активации"
                            cell={props => (
                                <td>
                                    <FieldDate date={props.dataItem.createDate} />
                                </td>
                            )}
                        />
                        <GridColumn field="id" title="Описание"
                            cell={props => (
                                <td>
                                    <SubscriptionActivationDescription data={props.dataItem} />
                                </td>
                            )}
                        />
                    </Grid>
                </LocalizationProvider>
            </div>
        );
    }

}