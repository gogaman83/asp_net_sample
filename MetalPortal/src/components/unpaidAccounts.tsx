﻿import * as React from 'react';
import * as jquery from 'jquery';
import Loader from '../utils/loader';
import DataSource from '../utils/dataSource';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import { LocalizationProvider } from '@progress/kendo-react-intl';

export default class UnpaidAccounts extends React.Component<any, any> {

    protected dataSource: any;

    constructor(props) {

        super(props);

        this.state = {
            filter: '',
            data: [],
            loading: true
        }

        this.dataSource = new DataSource({
            url: '/Data/GetUnpaidAccounts',
            onDataLoaded: this.onDataLoaded.bind(this)
        });
    }

    protected applyFilter = (event) => {
        this.setState({
            filter: event.target.value
        });
        this.loadData(event.target.value)
    }

    protected loadData(filter = null) {
        this.setState({
            loading: true
        });
        this.dataSource.load({
            filter: filter !== null ? filter : this.state.filter
        });
    }

    protected onDataLoaded(data) {
        this.setState({
            data: data,
            loading: false
        })
    }

    protected approvePayment(payment) {

        if (confirm(`Подтвердить оплату счета №${payment.id}?`)) {
            this.setState({
                loading: true
            });
            jquery.post('/Actions/SetAccountPaid', { paymentId: payment.id }, response => {
                this.loadData();
            });
        }
    }

    componentDidMount() {
        this.dataSource.load();
    }

    render() {
        return (
            <div>

                {this.state.loading && <Loader />}

                <div className="m_name_value_list" style={{ marginBottom: '2em' }}>
                    <div className="m_item">
                        <div className="m_name" style={{ width: 'auto', marginRight: '1em' }}>Поиск:</div>
                        <div className="m_value">
                            <input type="text" value={this.state.filter} onChange={this.applyFilter} />
                        </div>
                    </div>
                </div>

                <LocalizationProvider language="ru-RU">
                    <Grid
                        data={this.state.data}
                    >
                        <GridColumn field="id" title="Номер счета" />
                        <GridColumn field="user.userAccounting.payerName" title="Плательщик" />
                        <GridColumn field="service.name" title="Подписка" />
                        <GridColumn field="amount" title="Сумма" />
                        <GridColumn
                            field="id"
                            cell={props => (
                                <td>
                                    <input type="button" value="Подтвердить оплату" onClick={this.approvePayment.bind(this, props.dataItem)} />
                                </td>
                            )}
                        />

                    </Grid>
                </LocalizationProvider>
            </div>
        );
    }

}