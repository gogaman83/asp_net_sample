﻿import * as React from 'react';
import * as jquery from 'jquery';
import Loader from '../utils/loader';
import FormRow from '../utils/formRow';
import Form from '../utils/form';
import NumberFormat from 'react-number-format';

type FastOrderProps = {
    offerId: Number,
    onMount: any,
    onSuccessOK: any
}

export default class FastOrder extends React.Component<FastOrderProps, any> {

    protected rendered: any;
    protected orderForm: any;

    constructor(props) {

        super(props);

        this.state = {
            loading: false,
            createdOrder: null,
            offer: null,
            order: {
                quantity: 0,
                comment: '',
                name: window.metal.data.user ? window.metal.data.user.name : '',
                phone: window.metal.data.user ? window.metal.data.user.phoneNumber : '',
                email: window.metal.data.user ? window.metal.data.user.email : ''
            }
        }

        this.orderForm = React.createRef();
    }

    componentDidMount() {
        this.setState({
            loading: true
        })
        jquery.get('/Data/GetOffer', { offerId: this.props.offerId }, offer => {
            this.setState({
                offer: offer,
                loading: false
            })
        })
    }

    componentDidUpdate() {
        if (this.props.onMount && this.state.offer && !this.rendered) {
            this.props.onMount();
            this.rendered = true;
        }
    }

    orderChanged(e) {
        const target = e.target as HTMLInputElement;

        var order = { ...this.state.order };

        order[target.name] = target.value;

        this.setState({
            order: order
        });
    }

    orderPhoneChanged(e) {
        var order = { ...this.state.order };
        order.phone = e.value;
        this.setState({
            order: order
        });
    }

    orderQuantityChanged(e) {
        var order = { ...this.state.order };
        order.quantity = e.formattedValue;
        this.setState({
            order: order
        });
    }

    trySubmit() {
        this.setState({
            loading: true
        })
        jquery.post('/Actions/CreateOrderFromOffer', { offerId: this.props.offerId, order: this.state.order, quantity: this.state.order.quantity }, response => {
            if (response.success) {
                this.setState({
                    createdOrder: response.data,
                    loading: false
                })
                this.props.onMount();
            }
            else {
                this.orderForm.current.processErrors(response);
                this.setState({
                    loading: false
                })
            }

        })
    }

    onSuccessOK() {
        if (this.props.onSuccessOK)
            this.props.onSuccessOK();
    }

    render() {
        return (
            <div className="fast_order">

                {this.state.loading && <Loader />}

                {this.state.createdOrder &&
                    <div className="success">
                        Заявка успешно создана
                        <input value="ОК" type="button" onClick={this.onSuccessOK.bind(this)} />
                    </div>
                }

                {this.state.offer && !this.state.createdOrder && <Form ref={this.orderForm}>
                    <FormRow title="Тип товара: ">
                            <span className="value">{this.state.offer.catalogue.name}</span>
                    </FormRow>
                    <FormRow title="Марка стали: ">
                        <span className="value">{this.state.offer.steelGrade}</span>
                    </FormRow>
                    <FormRow title="Размер: ">
                        <span className="value">{this.state.offer.size}</span>
                    </FormRow>
                    <FormRow title="Цена: ">
                        <span className="value">{this.state.offer.price} руб.</span>
                    </FormRow>
                    <FormRow title="Поставщик: ">
                        <span className="value">{this.state.offer.vendor.name}</span>
                    </FormRow>
                    <FormRow name="name" title="Ваше имя" required={true}>
                        <input value={this.state.order.name} type="text" name="name" onChange={this.orderChanged.bind(this)} />
                    </FormRow>
                    <FormRow name="phone" title="Номер телефона" required={true}>
                        <NumberFormat format="+7 (###) ###-##-##" value={this.state.order.phone} type="text" name="phone" onValueChange={this.orderPhoneChanged.bind(this)} />
                    </FormRow>
                    <FormRow name="email" title="Email" required={false}>
                        <input value={this.state.order.email} type="text" name="email" onChange={this.orderChanged.bind(this)} />
                    </FormRow>
                    <FormRow name="quantity" title="Количество" required={true}>
                        <NumberFormat value={this.state.order.quantity} decimalSeparator="," type="text" name="quantity" onValueChange={this.orderQuantityChanged.bind(this)} />
                    </FormRow>
                    <FormRow name="comment" title="Примечание" required={false}>
                        <textarea style={{ resize: 'none' }} value={this.state.order.comment} name="comment" onChange={this.orderChanged.bind(this)} />
                    </FormRow>
                    <FormRow title="" >
                        <input value="Отправить" type="button" onClick={this.trySubmit.bind(this)} />
                    </FormRow>
                    </Form>
                }

            </div>
        );
    }

}