"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var jquery = require("jquery");
var kendo_react_dialogs_1 = require("@progress/kendo-react-dialogs");
var AddSubscription = /** @class */ (function (_super) {
    __extends(AddSubscription, _super);
    function AddSubscription(props) {
        var _this = _super.call(this, props) || this;
        _this.chooseService = function (id) {
            _this.setState({
                serviceId: id
            });
        };
        _this.choosePaymentType = function (id) {
            _this.setState({
                paymentTypeId: id
            });
        };
        _this.processPayment = function () {
            alert('payMeeee:' + _this.state.paymentTypeId);
        };
        _this.state = {
            services: [],
            paymentTypes: [],
            serviceId: null,
            paymentTypeId: null,
            successWindow: false
        };
        return _this;
    }
    AddSubscription.prototype.componentDidMount = function () {
        var _this = this;
        jquery.get("/Data/GetPaidServices", function (services) {
            jquery.get("/Data/GetPaymentTypes", function (paymentTypes) {
                _this.setState({
                    services: services,
                    paymentTypes: paymentTypes
                });
            });
        });
    };
    AddSubscription.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", null,
            React.createElement("div", { className: "m_AddSubscription__services" }, this.state.services.map(function (x) {
                return React.createElement("div", { className: "m_AddSubscription__service" + (x.id == _this.state.serviceId ? ' active' : ''), key: x.id, onClick: _this.chooseService.bind(_this, x.id) },
                    React.createElement("div", null,
                        React.createElement("div", { className: "m_AddSubscription__service__name" }, x.name),
                        React.createElement("div", { className: "m_AddSubscription__service__price" },
                            x.price,
                            " \u0440\u0443\u0431."),
                        React.createElement("div", { className: "m_AddSubscription__service__old_price" },
                            x.priceOld,
                            " \u0440\u0443\u0431.")));
            })),
            this.state.serviceId &&
                React.createElement("div", { className: "m_AddSubscription__payment_types" },
                    React.createElement("h2", null, "\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0441\u043F\u043E\u0441\u043E\u0431 \u043E\u043F\u043B\u0430\u0442\u044B"),
                    this.state.paymentTypes.map(function (x) {
                        return React.createElement("div", { className: "m_AddSubscription__payment_type" + (x.id == _this.state.paymentTypeId ? ' active' : ''), key: x.id, onClick: _this.choosePaymentType.bind(_this, x.id) },
                            React.createElement("div", null,
                                React.createElement("div", { className: "m_AddSubscription__payment_type__name" }, x.description)));
                    })),
            this.state.paymentTypeId !== null &&
                React.createElement("div", { className: "m_AddSubscription__payment" },
                    React.createElement("input", { type: "button", value: "\u041F\u0440\u043E\u0434\u043E\u043B\u0436\u0438\u0442\u044C", onClick: this.processPayment.bind(this) })),
            this.state.successWindow &&
                React.createElement(kendo_react_dialogs_1.Window, { title: "Status", initialHeight: 350 }, "hdjfhdjfhdfhjh")));
    };
    return AddSubscription;
}(React.Component));
exports.default = AddSubscription;
//# sourceMappingURL=addSubscription.js.map