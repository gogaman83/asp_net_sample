﻿import * as React from 'react';
import * as jquery from 'jquery';
import Loader from '../utils/loader';
import UserAccounting from './userAccounting';
import Sign from './sign';

declare global {
    interface Window {
        metal: any;
    }
}

export default class AddSubscription extends React.Component<any, any> {

    userAccounting: any;

    constructor(props) {
        super(props);
        this.userAccounting = React.createRef();
        this.state = {
            services: [],
            paymentTypes: [],
            serviceId: null,
            paymentTypeId: null,
            successWindow: false,
            loading: true,
            step: 1,
            accountingFile: null,
            accountingEmail: null,
            agreementAccepted: false
        }
    }

    componentDidMount() {
        jquery.get("/Data/GetPaidServices", services => {
            jquery.get("/Data/GetPaymentTypes", paymentTypes => {
                this.setState({
                    services: services,
                    paymentTypes: paymentTypes,
                    loading: false
                });
            })
        });
    }

    chooseService = id => {
        this.setState({
            serviceId: id
        });
    }

    choosePaymentType = id => {
        this.setState({
            paymentTypeId: id,
            showLogin: !window.metal.data.user
        });
    }

    changeAcceptAgreement(event) {
        const target = event.target;
        this.setState({
            agreementAccepted: target.checked
        });
    }

    process = () => {
        if (this.state.step == 1) {
            if (this.state.paymentTypeId == 0) {
                this.setState({ loading: true });
                jquery.get("/payment/createOnline", {
                    serviceId: this.state.serviceId,
                    returnUrl: this.props.returnUrl
                }, response => {
                    window.location = response.confirmation.confirmation_url;
                });
            }
            else {
                this.setState({
                    step: 2
                });
            }
        }
        else if (this.state.step == 2) {
            this.userAccounting.current.save();
        }
        else if (this.state.step == 3) {
            alert('go');
        }
    }

    onAccountingSave() {

        this.setState({ loading: true });
        jquery.get("/payment/createBank", {
            serviceId: this.state.serviceId,
            returnUrl: this.props.returnUrl
        }, response => {
            if (response.success) {
                this.setState({
                    accountingFile: response.data.file,
                    accountingEmail: response.data.email,
                    step: 3,
                    loading: false
                });
            }
        });
    }

    onLoginSuccess() {
        this.setState({
            showLogin: false
        });
    }

    render() {

        return (
            <div>

                {this.state.loading &&
                    <Loader />
                }

                {this.state.step == 1 &&

                    <div>
                        <div className="m_AddSubscription__services">
                            {this.state.services.map(x =>
                                <div className={"m_AddSubscription__service" + (x.id == this.state.serviceId ? ' active' : '')} key={x.id} onClick={this.chooseService.bind(this, x.id)}>
                                    <div>
                                        <div className="m_AddSubscription__service__name">{x.name}</div>
                                        <div className="m_AddSubscription__service__price">{x.price} руб.</div>
                                        <div className="m_AddSubscription__service__old_price">{x.priceOld} руб.</div>
                                    </div>
                                </div>
                            )}
                        </div>

                        {this.state.serviceId &&
                            <div className="m_AddSubscription__payment_types">
                                <h2>Выберите способ оплаты</h2>
                                {this.state.paymentTypes.map(x =>
                                    <div className={"m_AddSubscription__payment_type" + (x.id == this.state.paymentTypeId ? ' active' : '')} key={x.id} onClick={this.choosePaymentType.bind(this, x.id)}>
                                        <div>
                                            <div className="m_AddSubscription__payment_type__name">{x.description}</div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        }
                    
                    {this.state.showLogin && <Sign buttonText="Продолжить" onSuccess={this.onLoginSuccess.bind(this)} />}

                    </div>
                }

                {this.state.step == 2 &&
                    <div>
                        <h3>Введите данные для выставления счета</h3>
                        <br/>
                        <UserAccounting ref={this.userAccounting} onSaveSuccess={this.onAccountingSave.bind(this)} />
                    </div>
                }

                {this.state.step == 3 &&
                    <div>
                    <h3>На почту {this.state.accountingEmail} выслан счет на оплату</h3>
                        <a href={this.state.accountingFile}>Скачать счет</a>
                    </div>
                }

                {this.state.paymentTypeId !== null && !this.state.showLogin && this.state.step === 1 &&
                    <div className="m_AddSubscription__payment">
                    <iframe src="/home/serviceAgreement?headless=1" style={{ height: "150px", width: "100%", marginBottom: "1em", borderWidth: "1px" }} />
                    <label style={{ fontWeight: "bold", marginTop: "2em" }}><input type="checkbox" checked={this.state.agreementAccepted} onChange={this.changeAcceptAgreement.bind(this)} />  Я принимаю условия соглашения</label>
                    </div>
                }

                {this.state.paymentTypeId !== null && !this.state.showLogin && this.state.step < 3 && this.state.agreementAccepted &&
                    <div className="m_AddSubscription__payment">
                        <input type="button" value="Продолжить" onClick={this.process.bind(this)} />
                    </div>
                }

            </div>
        );
    }
}