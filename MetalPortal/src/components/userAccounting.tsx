﻿import * as React from 'react';
import * as jquery from 'jquery';
import Loader from '../utils/loader';

export default class UserAccounting extends React.Component<any, any> {

    constructor(props) {

        super(props);

        this.state = {
            data: {},
            errors: {},
            loading: true
        }
    }

    componentDidMount() {

        jquery.get("/Data/GetUserAccounting", data => {
            this.setState({
                data: data,
                loading: false
            });
        });
    }

    save() {

        const data = { ...this.state.data };

        this.setState({
            loading: true
        });

        jquery.post(
            "/Actions/UpdateUserAccounting",
            data,
            response => {

                let errors: any = {};
                if (response.errors && response.errors.length) {
                    response.errors.forEach(x => {
                        errors[x.key] = x.errors.map(x => x.errorMessage).join(', ');
                    });
                }
                else {
                    data.id = response.data.id;
                    if (this.props.onSaveSuccess)
                        this.props.onSaveSuccess(data);
                }

                this.setState({
                    loading: false,
                    data: data,
                    errors: errors
                });
            }
        );
    }

    protected handleChange(event) {
        const name = event.target.getAttribute('name');
        const data = { ...this.state.data };
        data[name] = event.target.value;
        this.setState({
            data: data
        });
    }

    render() {

        let data = this.state.data;
        let errors = this.state.errors;

        const fields = {
            bankId: {
                title: 'БИК'
            },
            bankName: {
                title: 'Наименование банка'
            },
            paymentAccount: {
                title: 'Расчетный счет'
            },
            payerName: {
                title: 'Имя плательщика'
            },
            itn: {
                title: 'ИНН'
            },
            email: {
                title: 'Email для счета'
            }
        }

        const items = [];

        for (const field in fields) {
            items.push(
                <div className="m_item">
                    <div className="m_name required">{fields[field].title}:</div>
                    <div className="m_value">
                        <input name={field} type="text" value={data[field]} onChange={this.handleChange.bind(this)} />
                        {errors[field] && <span className="field-validation-error">{errors[field]}</span>}
                    </div>
                </div>
            )
        }

        return (
            <div>

                {this.state.loading &&
                    <Loader />
                }

                {Object.keys(this.state.data).length > 1 &&
                    <div className="m_name_value_list">
                        {items}
                    </div>
                }

            </div>
        );
    }
}