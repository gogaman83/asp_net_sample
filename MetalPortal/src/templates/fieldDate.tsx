﻿import * as React from 'react';

export default class FieldDate extends React.Component<any, any> {

    protected formatDate(d: any) {

        var month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [day, month, year].join('.');
    }

    render() {

        const date = this.formatDate(new Date(this.props.date));

        return (
            <span>
                {date}
            </span>
        );
    }
}