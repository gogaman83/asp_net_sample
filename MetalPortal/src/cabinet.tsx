﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Provider, connect } from 'react-redux';
import store from './store';

import AddSubscription from './components/addSubscription';
import UnpaidAccounts from './components/unpaidAccounts';
import Payments from './components/payments';
import Subscriptions from './components/subscriptions';

import { loadMessages } from '@progress/kendo-react-intl';

const messages = {
    "grid": {
        "noRecords": "Нет записей"
    }
}

loadMessages(messages, 'ru-RU');

//App
class App extends React.Component<any, any> {

    render() {

        const components = {
            AddSubscription, UnpaidAccounts, Payments, Subscriptions
        };

        const ComponentName = components[this.props.component];

        return (
            <div className={"m_" + this.props.component}>
                <ComponentName {...this.props.options} />
            </div>
        );
    }
}

//connect
var AppRedux = connect(
    store => {
        return {
            store: store
        };
    },
    dispatch => ({
        go: name => dispatch({
            type: 'GO',
            payload: name
        })
    })
)(App);


//mpsignal
declare global {
    interface Window { app: any }
}

window.app = {
    init: function (element: any, component: any, options: any) {
        let app = ReactDOM.render(
            <Provider store={store}>
                <AppRedux component={component} options={options} />
            </Provider>,
            element
        );
        return { app, store };
    }
}
