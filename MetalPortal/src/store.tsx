﻿import { createStore } from 'redux';

 let store:any = createStore(
    (state: any, action: any) => {
        switch (action.type) {
            case 'GO':
                return { ...state, name: action.payload };
            default:
                return state;
        }
    },
    {
        name: 'One'
    }
);

export default store;