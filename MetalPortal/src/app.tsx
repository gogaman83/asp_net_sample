﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider, connect } from 'react-redux';

import store from './store';

import './css/app.scss';
import './css/themes/default.scss';

import AddSubscription from './components/addSubscription';
import Sign from './components/sign';
import LoginPage from './components/loginPage';
import FastOrder from './components/fastOrder';

//App
class App extends React.Component<any, any> {

    render() {

        const components = {
            AddSubscription,
            LoginPage,
            FastOrder,
            Sign
        };

        const ComponentName = components[this.props.component];

        return (
            <div className={"m_" + this.props.component}>
                <ComponentName {...this.props.options} />
            </div>
        );
    }
}

//connect
var AppRedux = connect(
    store => {
        return {
            store: store
        };
    },
    dispatch => ({
        go: name => dispatch({
            type: 'GO',
            payload: name
        })
    })
)(App);


//mpsignal
declare global {
    interface Window { app: any }
}

window.app = {
    init: function (element: any, component: any, options: any) {
        let app = ReactDOM.render(
            <Provider store={store}>
                <AppRedux component={component} options={options} />
            </Provider>,
            element
        );
        return { app, store };
    }
}
