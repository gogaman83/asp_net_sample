﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class payment7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LimitSubscriptionOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ChangeDate = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ServiceActivationId = table.Column<int>(nullable: false),
                    OrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LimitSubscriptionOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LimitSubscriptionOrders_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LimitSubscriptionOrders_PaidServicesActivations_ServiceActiv~",
                        column: x => x.ServiceActivationId,
                        principalTable: "PaidServicesActivations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LimitSubscriptionOrders_OrderId",
                table: "LimitSubscriptionOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_LimitSubscriptionOrders_ServiceActivationId",
                table: "LimitSubscriptionOrders",
                column: "ServiceActivationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LimitSubscriptionOrders");
        }
    }
}
