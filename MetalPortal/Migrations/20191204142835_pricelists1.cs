﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class pricelists1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ErrorsJSON",
                table: "Pricelists",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PricelistId",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PricelistUrl",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_PricelistId",
                table: "Offers",
                column: "PricelistId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Pricelists_PricelistId",
                table: "Offers",
                column: "PricelistId",
                principalTable: "Pricelists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Pricelists_PricelistId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_PricelistId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ErrorsJSON",
                table: "Pricelists");

            migrationBuilder.DropColumn(
                name: "PricelistId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "PricelistUrl",
                table: "AspNetUsers");
        }
    }
}
