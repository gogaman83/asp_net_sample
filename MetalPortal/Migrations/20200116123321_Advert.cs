﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class Advert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AdvertId",
                table: "Uploads",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Adverts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ChangeDate = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Organization = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    CatalogueItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adverts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Adverts_Catalogue_CatalogueItemId",
                        column: x => x.CatalogueItemId,
                        principalTable: "Catalogue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Adverts_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdvertRaisings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ChangeDate = table.Column<DateTime>(nullable: true),
                    AdvertId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertRaisings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertRaisings_Adverts_AdvertId",
                        column: x => x.AdvertId,
                        principalTable: "Adverts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Uploads_AdvertId",
                table: "Uploads",
                column: "AdvertId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertRaisings_AdvertId",
                table: "AdvertRaisings",
                column: "AdvertId");

            migrationBuilder.CreateIndex(
                name: "IX_Adverts_CatalogueItemId",
                table: "Adverts",
                column: "CatalogueItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Adverts_UserId",
                table: "Adverts",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Uploads_Adverts_AdvertId",
                table: "Uploads",
                column: "AdvertId",
                principalTable: "Adverts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Uploads_Adverts_AdvertId",
                table: "Uploads");

            migrationBuilder.DropTable(
                name: "AdvertRaisings");

            migrationBuilder.DropTable(
                name: "Adverts");

            migrationBuilder.DropIndex(
                name: "IX_Uploads_AdvertId",
                table: "Uploads");

            migrationBuilder.DropColumn(
                name: "AdvertId",
                table: "Uploads");
        }
    }
}
