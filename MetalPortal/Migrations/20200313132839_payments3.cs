﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class payments3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PaymentId",
                table: "UserPaidServices",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserPaidServices_PaymentId",
                table: "UserPaidServices",
                column: "PaymentId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserPaidServices_Payments_PaymentId",
                table: "UserPaidServices",
                column: "PaymentId",
                principalTable: "Payments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserPaidServices_Payments_PaymentId",
                table: "UserPaidServices");

            migrationBuilder.DropIndex(
                name: "IX_UserPaidServices_PaymentId",
                table: "UserPaidServices");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "UserPaidServices");
        }
    }
}
