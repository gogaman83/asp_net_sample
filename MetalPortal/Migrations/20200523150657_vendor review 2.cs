﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class vendorreview2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VendorRatings_VendorReviews_ReviewId",
                table: "VendorRatings");

            migrationBuilder.DropIndex(
                name: "IX_VendorRatings_ReviewId",
                table: "VendorRatings");

            migrationBuilder.DropColumn(
                name: "ReviewId",
                table: "VendorRatings");

            migrationBuilder.AddColumn<int>(
                name: "RatingId",
                table: "VendorReviews",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VendorReviews_RatingId",
                table: "VendorReviews",
                column: "RatingId");

            migrationBuilder.AddForeignKey(
                name: "FK_VendorReviews_VendorRatings_RatingId",
                table: "VendorReviews",
                column: "RatingId",
                principalTable: "VendorRatings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VendorReviews_VendorRatings_RatingId",
                table: "VendorReviews");

            migrationBuilder.DropIndex(
                name: "IX_VendorReviews_RatingId",
                table: "VendorReviews");

            migrationBuilder.DropColumn(
                name: "RatingId",
                table: "VendorReviews");

            migrationBuilder.AddColumn<int>(
                name: "ReviewId",
                table: "VendorRatings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VendorRatings_ReviewId",
                table: "VendorRatings",
                column: "ReviewId");

            migrationBuilder.AddForeignKey(
                name: "FK_VendorRatings_VendorReviews_ReviewId",
                table: "VendorRatings",
                column: "ReviewId",
                principalTable: "VendorReviews",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
