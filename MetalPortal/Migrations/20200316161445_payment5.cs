﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class payment5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserPaidServices");

            migrationBuilder.CreateTable(
                name: "PaidServicesActivations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ChangeDate = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    ServiceId = table.Column<int>(nullable: true),
                    PaymentId = table.Column<int>(nullable: true),
                    ServiceState = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaidServicesActivations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaidServicesActivations_Payments_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "Payments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaidServicesActivations_PaidServices_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "PaidServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaidServicesActivations_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaidServicesActivations_PaymentId",
                table: "PaidServicesActivations",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_PaidServicesActivations_ServiceId",
                table: "PaidServicesActivations",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_PaidServicesActivations_UserId",
                table: "PaidServicesActivations",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaidServicesActivations");

            migrationBuilder.CreateTable(
                name: "UserPaidServices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ChangeDate = table.Column<DateTime>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    PaymentId = table.Column<int>(nullable: true),
                    ServiceId = table.Column<int>(nullable: true),
                    ServiceState = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPaidServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPaidServices_Payments_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "Payments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserPaidServices_PaidServices_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "PaidServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserPaidServices_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserPaidServices_PaymentId",
                table: "UserPaidServices",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPaidServices_ServiceId",
                table: "UserPaidServices",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPaidServices_UserId",
                table: "UserPaidServices",
                column: "UserId");
        }
    }
}
