﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class offers1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderItems_Products_ProductId",
                table: "OrderItems");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.RenameColumn(
                name: "ProductId",
                table: "OrderItems",
                newName: "OfferId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderItems_ProductId",
                table: "OrderItems",
                newName: "IX_OrderItems_OfferId");

            migrationBuilder.AlterColumn<string>(
                name: "SteelGrade",
                table: "Offers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Size",
                table: "Offers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Article",
                table: "Offers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_Article",
                table: "Offers",
                column: "Article");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_Size",
                table: "Offers",
                column: "Size");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_SteelGrade",
                table: "Offers",
                column: "SteelGrade");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderItems_Offers_OfferId",
                table: "OrderItems",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderItems_Offers_OfferId",
                table: "OrderItems");

            migrationBuilder.DropIndex(
                name: "IX_Offers_Article",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_Size",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_SteelGrade",
                table: "Offers");

            migrationBuilder.RenameColumn(
                name: "OfferId",
                table: "OrderItems",
                newName: "ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderItems_OfferId",
                table: "OrderItems",
                newName: "IX_OrderItems_ProductId");

            migrationBuilder.AlterColumn<string>(
                name: "SteelGrade",
                table: "Offers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Size",
                table: "Offers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Article",
                table: "Offers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Article = table.Column<string>(nullable: true),
                    CatalogueId = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    MetalType = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: true),
                    Quantity = table.Column<double>(nullable: false),
                    Size = table.Column<string>(nullable: true),
                    SteelGrade = table.Column<string>(nullable: true),
                    VendorId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Catalogue_CatalogueId",
                        column: x => x.CatalogueId,
                        principalTable: "Catalogue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_AspNetUsers_VendorId",
                        column: x => x.VendorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_Article",
                table: "Products",
                column: "Article");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CatalogueId",
                table: "Products",
                column: "CatalogueId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_Size",
                table: "Products",
                column: "Size");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SteelGrade",
                table: "Products",
                column: "SteelGrade");

            migrationBuilder.CreateIndex(
                name: "IX_Products_VendorId",
                table: "Products",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderItems_Products_ProductId",
                table: "OrderItems",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
