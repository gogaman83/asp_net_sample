﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MetalPortal.Migrations
{
    public partial class reviews2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Publish",
                table: "VendorReviews",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Publish",
                table: "VendorReviews");
        }
    }
}
