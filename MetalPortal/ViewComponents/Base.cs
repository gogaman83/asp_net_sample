﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MetalPortal.Core;

namespace MetalPortal
{
    public abstract class BaseViewComponent : ViewComponent
    {
        protected Globals globals;
        public string guid;

        public BaseViewComponent(Globals globals)
        {
            this.globals = globals;
            this.guid = System.Guid.NewGuid().ToString();
        }
    }
}