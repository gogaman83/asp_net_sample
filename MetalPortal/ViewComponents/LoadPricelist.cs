﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MetalPortal.Core;

namespace MetalPortal
{
    public class LoadPricelistViewComponent : BaseViewComponent
    {
        public LoadPricelistViewComponent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(this);
        }
    }
}