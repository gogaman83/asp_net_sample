﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MetalPortal.Core;

namespace MetalPortal
{
    public abstract class EditCardComponent : BaseViewComponent
    {
        public string UpdateAction { get; set; }

        public EditCardComponent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
} 