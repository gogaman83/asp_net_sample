﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace MetalPortal
{
    public class AdvertViewComponent : BaseViewComponent
    {
        public Advert Advert { get; set; }

        public AdvertViewComponent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
        public async Task<IViewComponentResult> InvokeAsync(int advertId)
        {
            this.Advert = this.globals.db.Adverts.Where(x => x.Id == advertId).Include(x => x.User).Include(x => x.CatalogueItem).Include(x => x.Uploads).FirstOrDefault();

            return View(this);
        }
    }
}