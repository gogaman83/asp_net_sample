﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MetalPortal.Core;
using MetalPortal.Core.Models;

namespace MetalPortal
{
    public class OffersViewComponent : BaseViewComponent
    {
        public int? UserId { get; set; }
        public bool ForEdit { get; set; } 
        public string SearchText { get; set; }
        public OffersViewComponent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
        public async Task<IViewComponentResult> InvokeAsync(int? userId, bool forEdit = false, string searchText = null)
        {
            this.UserId = userId.HasValue ? userId.Value : (this.globals.user != null ? (int?)this.globals.user.Id : null);
            this.ForEdit = forEdit && this.globals.user != null && this.UserId == this.globals.user.Id;
            this.SearchText = searchText;
            return View(this);
        }
    }
}