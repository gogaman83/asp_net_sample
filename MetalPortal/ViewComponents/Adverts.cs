﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MetalPortal.Models;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace MetalPortal
{
    public class AdvertsViewComponent : BaseViewComponent
    {
        public List<Advert> Adverts { get; set; }
        public bool EditMode { get; set; }

        public AdvertsViewComponent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
        public async Task<IViewComponentResult> InvokeAsync(bool editMode)
        {
            var query = globals.db.Adverts.Where(x => !x.Deleted);

            if (editMode && globals.user != null)
            {
                query = query.Where(x => x.UserId == globals.user.Id);
                this.EditMode = true;
            }

            this.Adverts = query.Include(x => x.CatalogueItem).Include(x => x.Uploads).Include(x => x.Raisings).ToList().OrderBy(x => x.SortingOrder).ToList();

            this.Adverts.ForEach(a => a.Uploads = a.Uploads.Where(x => !x.Deleted).ToList());

            return View(this);
        }
    }
}