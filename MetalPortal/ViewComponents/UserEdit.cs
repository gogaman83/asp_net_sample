﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MetalPortal.Core;
using MetalPortal.Core.Models;

namespace MetalPortal
{
    public class UserEditViewComponent : EditCardComponent
    {
        public User EditedUser { get; set; }

        public UserEditViewComponent(Globals globals) : base(globals)
        {
            this.globals = globals;
            this.UpdateAction = "UpdateUser";
        }
        public async Task<IViewComponentResult> InvokeAsync(int? userId = null)
        {
            this.EditedUser = globals.db.Users.Find(userId.HasValue ? userId : globals.user.Id);
            return View(this);
        }
    }
}