﻿const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        app: "./src/app.tsx",
        cabinet: "./src/cabinet.tsx"
    },
    output: {
        path: path.resolve(__dirname, "wwwroot/dist"),
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                use: {
                    loader: "ts-loader"
                },
                test: /\.(ts|tsx)?$/,
                exclude: /node_modules/
            },
            {
                use: {
                    loader: "babel-loader"
                },
                test: /\.(js|jsx)?$/,
                exclude: /node_modules/
            },
            {
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ],
                test: /\.scss$/,
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css'
        })
    ],
    resolve: {
        extensions: ['.tsx', '.ts', '.jsx', '.js']
    }
}