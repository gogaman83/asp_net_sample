﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MetalPortal.Core;
using MetalPortal.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MetalPortal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
             });

            services.AddHttpContextAccessor();
            services.AddDbContext<ApplicationContext>(opts => opts.UseMySql(Configuration["ConnectionStrings:Connection1gb"]));
            services.AddIdentity<User, IdentityUserRole>().AddEntityFrameworkStores<ApplicationContext>();
            services.AddScoped<UploadsManager, UploadsManager>();
            services.AddScoped<Globals, Globals>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 1;
                options.Password.RequiredUniqueChars = 1;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/login";
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddMvc().AddJsonOptions(opt => {
                opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.Cookie.Name = ".MyApp.Session";
                options.IdleTimeout = TimeSpan.FromSeconds(3600);
                options.Cookie.IsEssential = true;
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseSession();

            //Роутинг
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "login",
                    template: "login",
                    defaults: new { controller = "Account", action = "Login" }
                );

                routes.MapRoute(
                    name: "logout",
                    template: "logout",
                    defaults: new { controller = "Account", action = "Logout" }
                );

                routes.MapRoute(
                    name: "register",
                    template: "register",
                    defaults: new { controller = "Account", action = "Register" }
                );

                routes.MapRoute(
                    name: "newOrder",
                    template: "orders/new",
                    defaults: new { controller = "Home", action = "OrderNew" }
                );

                routes.MapRoute(
                    name: "products",
                    template: "products",
                    defaults: new { controller = "Home", action = "Products" }
                );

                routes.MapRoute(
                    name: "cabinet",
                    template: "cabinet",
                    defaults: new { controller = "Cabinet", action = "Index" }
                );

                routes.MapRoute(
                    name: "cabinet/orders",
                    template: "cabinet/orders",
                    defaults: new { controller = "Cabinet", action = "Orders" }
                );

                routes.MapRoute(
                    name: "cabinet/adverts",
                    template: "cabinet/adverts",
                    defaults: new { controller = "Cabinet", action = "Adverts" }
                );

                routes.MapRoute(
                    name: "cabinet/offers",
                    template: "cabinet/offers",
                    defaults: new { controller = "Cabinet", action = "Offers" }
                );

                routes.MapRoute(
                    name: "cabinet/offers/load",
                    template: "cabinet/offers/load",
                    defaults: new { controller = "Cabinet", action = "LoadPricelist" }
                );

                routes.MapRoute(
                    name: "cabinet/vendors",
                    template: "cabinet/vendors",
                    defaults: new { controller = "Cabinet", action = "Vendors" }
                );

                routes.MapRoute(
                    name: "cabinet/unpaid_accounts",
                    template: "cabinet/unpaid_accounts",
                    defaults: new { controller = "Cabinet", action = "UnpaidAccounts" }
                );

                routes.MapRoute(
                    name: "cabinet/payments",
                    template: "cabinet/payments",
                    defaults: new { controller = "Cabinet", action = "Payments" }
                );

                routes.MapRoute(
                    name: "cabinet/subscriptions",
                    template: "cabinet/subscriptions",
                    defaults: new { controller = "Cabinet", action = "Subscriptions" }
                );

                routes.MapRoute(
                    name: "offers",
                    template: "offers/{category1?}/{category2?}/{category3?}/{category4?}",
                    defaults: new { controller = "Navigation", action = "GetList", root = "offers" }
                );

                routes.MapRoute(
                    name: "orders",
                    template: "orders/{category1?}/{category2?}/{category3?}/{category4?}",
                    defaults: new { controller = "Navigation", action = "GetList", root = "orders" }
                );

                routes.MapRoute(
                    name: "order",
                    template: "order/{id}",
                    defaults: new { controller = "Home", action = "Order"}
                );

                routes.MapRoute(
                    name: "offer",
                    template: "offer/{id}",
                    defaults: new { controller = "Home", action = "Offer" }
                );

                routes.MapRoute(
                    name: "vendor",
                    template: "vendor/{id}",
                    defaults: new { controller = "Home", action = "Vendor" }
                );

                routes.MapRoute(
                    name: "adverts",
                    template: "adverts",
                    defaults: new { controller = "Home", action = "Adverts" }
                );

                routes.MapRoute(
                    name: "adverts/add",
                    template: "adverts/add",
                    defaults: new { controller = "Home", action = "AdvertsAdd" }
                );

                routes.MapRoute(
                    name: "advert",
                    template: "advert/{id}",
                    defaults: new { controller = "Home", action = "Advert" }
                );

                routes.MapRoute(
                    name: "adverts",
                    template: "adverts",
                    defaults: new { controller = "Home", action = "Adverts" }
                );

                routes.MapRoute(
                    name: "subscribe",
                    template: "subscribe",
                    defaults: new { controller = "Home", action = "Subscribe" }
                );

                routes.MapRoute(
                    name: "payment/createOnline",
                    template: "payment/createOnline",
                    defaults: new { controller = "Payment", action = "CreateOnline" }
                );

                routes.MapRoute(
                    name: "payment/notification",
                    template: "payment/notification",
                    defaults: new { controller = "Payment", action = "Notify" }
                );

                routes.MapRoute(
                    name: "payment/succeeded",
                    template: "payment/succeeded",
                    defaults: new { controller = "Home", action = "OnPaymentSuccess" }
                );

                routes.MapRoute(
                    name: "search",
                    template: "search",
                    defaults: new { controller = "Home", action = "Search" }
                );

                routes.MapRoute(
                    name: "contacts",
                    template: "contacts",
                    defaults: new { controller = "Home", action = "Contacts" }
                );


                //routes.MapRoute(
                //    name: "index",
                //    template: "{controller=Catalogue}/{action=GetList}");

                routes.MapRoute(
                     name: "default",
                     template: "{controller}/{action}",
                     defaults: new { controller = "Navigation", action = "GetList", root = "offers" }
                );
            });
        }
    }
}
