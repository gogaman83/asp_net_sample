﻿//Компонент
(function ($) {
    var parentWidget = window.kendo.ui.GuidedWidget;
    var Component = parentWidget.extend({
        description: "Component",
        catalogueItems: null,
        catalogueIdDropDownEditor: null,
        init: function (element, options) {

            parentWidget.fn.init.call(this, element, options);

            var widget = this;

            widget.catalogueItems = metal.data.catalogueItems;

            this.catalogueIdDropDownEditor = function (container, options) {
                $('<input required name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataTextField: "name",
                        dataValueField: "id",
                        dataSource: widget.catalogueItems,
                        valuePrimitive: true
                    });
            };

            widget.bind('ready', function () {
                widget.onReady();
            });

            widget.renderHTML();
        },
        onReady: function () {

        },
        renderHTML: function () {
            var widget = this;
            if (widget.options.loadTemplate) {
                $.get('/templates/' + widget.options.name + '.htm', {}, function (response) {
                    $("body").append(response);
                    widget.element.html('<div class="m_' + widget.options.name + '">' + $('#' + widget.options.name).html() + '</div>');
                    widget.trigger('ready');
                });
            }
        },
        options: {
            name: "Component",
            loadTemplate: true,
            isPopup: false
        }
    });
    metal.registerWidget(Component);
})(jQuery);

//React компонент
(function ($) {
    var parentWidget = metal.widgets.Component;
    var ComponentReact = parentWidget.extend({
        description: "ComponentReact",
        window: null,
        init: function (element, options) {
            parentWidget.fn.init.call(this, element, options);
            var widget = this;

            //Инициализируем окно
            if (widget.options.isPopup) {
                widget.window = widget.element.kendoWindow({
                    visible: false,
                    title: widget.options.description,
                    modal: true
                }).data('kendoWindow');
            }

            //Инициализируем компонент
            window.app.init(widget.element[0], widget.options.componentName, widget.options.componentProps);
        },
        open: function () {
            if (this.window)
                this.window.center().open();
        },
        close: function () {
            if (this.window)
                this.window.close();
        },
        options: {
            name: "ComponentReact",
            description: '',
            isPopup: false,
            componentName: null,
            componentProps: {}
        }
    });
    metal.registerWidget(ComponentReact);
})(jQuery);

//Форма
(function ($) {
    var parentWidget = metal.widgets.Component;
    var ComponentForm = parentWidget.extend({
        description: "ComponentForm",
        window: null,
        form: null,
        bindedData: null,
        init: function (element, options) {
            parentWidget.fn.init.call(this, element, options);
            var widget = this;

            //Инициализируем окно
            if (widget.options.popupMode) {
                widget.window = widget.element.kendoWindow({
                    visible: false,
                    title: widget.options.description,
                    modal: true
                }).data('kendoWindow');
            }

            //События
            widget.bind('templateLoaded', function () {

                widget.form = widget.element.find('form');

                if (widget.bindedObject)
                    kendo.bind(widget.element, widget.bindedObject);

                if (widget.options.action) {
                    widget.form.submit(function (e) {
                        e.preventDefault();
                        widget.submit();
                    });
                }
            });

            widget.bind('dataBound', function () {
                if (widget.window)
                    widget.open();
            });

            widget.bind('success', function () {
                if (widget.window)
                    widget.window.close();
                if (widget.options.success)
                    widget.options.success.call(widget);
            });
        },
        submit: function () {
            var widget = this;

            var data = null;

            if (widget.bindedObject)
                data = widget.bindedObject.data.toJSON();
            else
                data = widget.options.useFormData ? new FormData(widget.form[0]) : widget.form.serializeToObject();

            widget.form.showLoader();
            (new metal.actions.action(widget.options.action)).post(data, widget.options.type)
                .then(function (response) {
                    metal.ui.message(response.message, function () {
                        widget.trigger('success', response);
                    });
                })
                .catch(function (result) {
                    if (result.errors) {
                        widget.form.find('.field-validation-error').removeClass('field-validation-error').addClass('field-validation-valid').html('').hide();
                        result.errors.forEach(function (error) {
                            widget.form.find('.field-validation-valid[data-valmsg-for="' + error.key + '"]').removeClass('field-validation-valid').addClass('field-validation-error').html(error.value.errors.map(function (x) {
                                return x.errorMessage;
                            }).join('<br/>')).show();
                        });
                    }
                    else if (response.message) {
                        metal.ui.error(response.message);
                    }
                })
                .finally(function () {
                    widget.form.hideLoader();
                });
        },  
        //Заполняем контент
        load: function (callback) {
            var widget = this;
            if (widget.options.hasStaticTemplate) {
                $.get('/templates/' + widget.options.name + '.htm', {}, function (response) {
                    $("body").append(response);
                    widget.element.html('<div class="m_' + widget.options.name + '">' + $('#' + widget.options.name).html() + '</div>');
                    if (callback)
                        callback.call(widget);
                    widget.trigger('templateLoaded');
                });
            }
        },
        open: function () {
            if (this.window)
                this.window.center().open();
        },
        options: {
            name: "ComponentForm",
            description: '',
            popupMode: false,
            action: null,
            useFormData: false,
            hasStaticTemplate: true,
            type: 'json'
        }
    });
    metal.registerWidget(ComponentForm);
})(jQuery);

//Форма отзыва на заказ
(function ($) {
    var parentWidget = metal.widgets.ComponentForm;
    var OrderFeedbackForm = parentWidget.extend({
        description: "OrderFeedbackForm",
        init: function (element, options) {
            parentWidget.fn.init.call(this, element, options);
            var widget = this;

            widget.bind('templateLoaded', function () {
                widget.form.find('[name="OrderId"]').val(widget.options.orderId);
            });
        },
        options: {
            name: "OrderFeedbackForm",
            description: 'Ответ на заявку',
            popupMode: true,
            action: 'AddOrderFeedback'
        }
    });
    metal.registerWidget(OrderFeedbackForm);
})(jQuery);

//Форма добавления объявления
(function ($) {
    var parentWidget = metal.widgets.ComponentForm;
    var AddAdvertForm = parentWidget.extend({
        description: "AddAdvertForm",
        advert: null,
        init: function (element, options) {
            parentWidget.fn.init.call(this, element, options);
            var widget = this;

            widget.bind('templateLoaded', function () {
                if (widget.options.advertId) {
                    $.get('/Data/GetAdvert', { advertId: widget.options.advertId }, function (response) {
                        widget.bindAdvert(response);
                    });
                }
                else {
                    widget.bindAdvert({
                        type: 0,
                        name: metal.data.user ? metal.data.user.name : '',
                        email: metal.data.user ? metal.data.user.email : '',
                        phone: metal.data.user ? metal.data.user.phoneNumber : '',
                        organization: metal.data.user ? metal.data.user.organization : ''
                    });
                }
            });

            widget.bind('success', function () {
                if (widget.options.reloadOnSuccess)
                    widget.load();
            });
        },
        bindAdvert: function (advert) {
            var widget = this;
            widget.advert = kendo.observable(advert);
            widget.advert.types = metal.enums.advertTypes.toDataSource();
            widget.advert.catalogueItems = metal.data.catalogueItems;
            kendo.bind(widget.element, widget.advert);
            widget.trigger('dataBound');
        },
        options: {
            name: "AddAdvertForm",
            description: 'Добавление объявления',
            action: 'CreateOrUpdateAdvert',
            advertId: null,
            useFormData: true,
            reloadOnSuccess: true
        }
    });
    metal.registerWidget(AddAdvertForm);
})(jQuery);

//Форма отзыва поставщика
(function ($) {
    var parentWidget = metal.widgets.ComponentForm;
    var VendorFeedbackForm = parentWidget.extend({
        description: "VendorFeedbackForm",
        init: function (element, options) {
            parentWidget.fn.init.call(this, element, options);
            var widget = this;

            widget.bind('templateLoaded', function () {
                widget.form.find('[name="vendorId"]').val(widget.options.vendorId);
                widget.kendoRating = widget.form.find('[name="rating"]').kendoRating().data('kendoRating');
            });

            widget.bind('success', function () {
                widget.bindedObject.set('isSelfReviewWrapperVisible', false);
                widget.bindedObject.reviews.read();
            });

            widget.signForm = $("<div></div>").appendTo('body').kendoComponentReact({
                isPopup: true,
                componentName: 'Sign',
                componentProps: {
                    onSuccess: function () {
                        widget.signForm.close();
                        widget.submit();
                    }
                }
            }).data('kendoComponentReact');

            widget.bindedObject = kendo.observable({
                data: {
                    vendorId: widget.options.vendorId,
                    message: '',
                    name: '',
                    rating: 3
                },
                isSelfReviewVisible: false,
                isShowReviewVisible: true,
                isSelfReviewWrapperVisible: widget.options.canBeReviewed,
                showReview: function (e) {
                    e.preventDefault();
                    this.set('isSelfReviewVisible', true);
                    this.set('isShowReviewVisible', false);
                },
                send: function () {
                    if (metal.data.user)
                        widget.submit();
                    else {
                        widget.signForm.open();
                    }
                },
                reviews: new kendo.data.DataSource({
                    schema: {
                        model: {
                            id: "id"
                        }
                    },
                    batch: true,
                    transport: {
                        read: '/Data/GetVendorReviews',
                        parameterMap: function (options, operation) {
                            if (operation === "read") {
                                return { vendorId: widget.options.vendorId };
                            }
                        }
                    }
                }),
                reviewsBound: function () {
                    kendo.init(widget.element);
                }
            });

            widget.load();
        },
        options: {
            name: "VendorFeedbackForm",
            description: 'Отзыв о поставщике',
            popupMode: false,
            vendorId: null,
            action: 'AddVendorFeedback',
            type: 'form',
            canBeReviewed: true
        }
    });
    metal.registerWidget(VendorFeedbackForm);
})(jQuery);

//Поисковая строка
(function ($) {
    var parentWidget = window.kendo.ui.GuidedWidget;
    var Search = parentWidget.extend({
        description: "Search",
        autoComplete: null,
        showButton: null,
        init: function (element, options) {

            parentWidget.fn.init.call(this, element, options);

            var widget = this;

            widget.element.html('<button style="visibility:hidden">Перейти</button><div><input /></div>');

            widget.showButton = widget.element.find('button').click(function (e) {
                location.href = "/search?text=" + widget.autoComplete.value();
            });

            widget.autoComplete = widget.element.find('input').kendoAutoComplete({
                placeholder: "Найти товар или услугу...",
                minLength: 3,
                dataTextField: "shortText",
                //headerTemplate: '<div class="dropdown-header k-widget k-header">' +
                //    '<span>Photo</span>' +
                //    '<span>Contact info</span>' +
                //    '</div>',
                //footerTemplate: 'Найдено #: instance.dataSource.total() # items found',
                noDataTemplate: 'Ничего не найдено',
                template: '<div class="search_result_item">' +
                    '<a href="/#: data.url #">#: data.shortText #</a><span>#: data.typeName #</span>' +
                    '</div>',
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "/Data/Search"
                        },
                        parameterMap: function (data, type) {
                            return {
                                search: data.filter.filters.length ? data.filter.filters[0].value : null,
                                take: widget.options.take
                            };
                        }
                    }
                },
                dataBound: function () {
                    var ds = this.dataSource;
                    widget.showButton.css('visibility', ds.data().some(function (x) { return x.type === 2 }) ? 'visible' : 'hidden');
                },
                height: 300
            }).data("kendoAutoComplete");

        },
        options: {
            name: "Search",
            take: 20
        }
    });
    metal.registerWidget(Search);
})(jQuery);