﻿var metal = {
    widgets: {},
    ui: {},
    actions: {},
    enums: {},
    application: {},
    formats: {},
    templates: {},
    app: {}
};

//widgets
metal.registerWidget = function (widget) {
    window.kendo.ui.plugin(widget);
    var prototype = widget.prototype;
    if (metal[metal.widgets[prototype.options.name]]) {
        console.error("Widget " + prototype.options.name + " already registered");
    }
    else {
        metal.widgets[prototype.options.name] = widget;
        var widgetName = "metal" + prototype.options.name;
        jQuery.fn[widgetName] = function (options) {
            var widgetInstance = new widget(this, options);
            this.data(widgetName, widgetInstance);
            return widgetInstance;
        };
    }
};

metal.widgets.createInstance = function (widgetName, options) {
    return $('<div></div>').appendTo('body')['metal' + widgetName](options);
};

(function ($) {
    var kendo = window.kendo,
        ui = kendo.ui,
        parentWidget = ui.Widget;
    var kendoGuidedWidget = parentWidget.extend({
        guid: "",
        description: "Guided widget",
        init: function (element, options) {
            //element.wrap("<div class='" + this.options.name + "Component'></div>");
            element.addClass(this.options.name + "Component");
            parentWidget.fn.init.call(this, element, options);
        },
        options: {
            name: "GuidedWidget",
        }
    });
    ui.plugin(kendoGuidedWidget);
})(jQuery);

//ui
metal.ui.message = function (message, close) {
    $("<div>" + message + "</div>").appendTo('body').kendoDialog({
        title: false,
        close: function (e) {
            if (close)
                close();
        },
        actions: [{
            text: "OK",
            action: function (e) {
                return true;
            },
            primary: true
        }]
    });
}
metal.ui.error = function (message, close) {
    $("<div style='color:red'>" + message + "</div>").appendTo('body').kendoDialog({
        title: false,
        close: function (e) {
            if (close)
                close();
        },
        actions: [{
            text: "OK",
            action: function (e) {
                return true;
            },
            primary: true
        }]
    });
};
metal.ui.confirm = function (message, yes, no) {
    if (confirm(message) && yes)
        yes();
    else if (no)
        no();
};

//application
metal.application.openPage = function (url) {
    window.location = url;
};

//actions
metal.actions.action = kendo.Class.extend({
    name: null,
    targetId: null,
    guid: null,
    init: function (name, targetId) {

        targetId = targetId || null;

        this.name = name;
        this.targetId = targetId;
        this.guid = Date.now();
    },
    postForm: function (data) {
        return this.post(data, 'form');
    },
    post: function (data, type) {

        data = data || {};
        type = type || 'json';
        var action = this;

        var ajaxOptions = {
            url: "/Actions/" + action.name,
            type: "POST",
            dataType: 'json',
            traditional: true
        };

        if (data instanceof FormData) {

            if (!data.get('Id') && this.targetId)
                data.append('Id', this.targetId);

            ajaxOptions = $.extend(ajaxOptions, {
                contentType: false,
                processData: false,
                data: data
            });
        }
        else if (type === 'json') {

            if (!data.Id && this.targetId)
                data.Id = this.targetId;

            ajaxOptions = $.extend(ajaxOptions, {
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data)
            });
        }
        else if (type === 'form') {

            if (!data.Id && this.targetId)
                data.Id = this.targetId;

            ajaxOptions = $.extend(ajaxOptions, {
                data: data
            });
        }

        return new Promise(function (resolve, reject) {

            var errorHandler = function (response, xhr, status, error) {

                if (response && !response.message && response.data) {
                    var strings = [];
                    for (var field in response.data)
                        strings.push(response.data[field]);
                    response.message = strings.join("<br/>");
                }

                reject(response);
            };

            jQuery.ajax(ajaxOptions).done(function (response) {
                if (response.success)
                    resolve(response);
                else
                    errorHandler(response);
            }).fail(function (xhr, status, error) {
                errorHandler(null, xhr, status, error);
            });
        })
    }
});

//enums
metal.enums.baseClass = kendo.Class.extend({
    enumItems: [],
    init: function (enumItems) {
        var that = this;
        this.enumItems = enumItems;
        this.enumItems.forEach(function (enumItem) {
            that[enumItem.Name] = enumItem;
        });
    },
    get: function (id) {
        if (id !== null && id !== undefined) {
            return this.enumItems.first(function (enumItem) {
                return enumItem.Id == id;
            });
        }
        else
            return null;
    },
    find: function (name) {
        return this.enumItems.first(function (enumItem) {
            return enumItem.Name == name;
        });
    },
    findId: function (name) {
        return this.find(name).Id;
    },
    toArray: function (include) {
        include = include || false;
        var that = this;
        var enumItems = that.enumItems;
        if (include) {
            enumItems = [];
            that.enumItems.forEach(function (x) {
                if (x && include.indexOf(x.Id) >= 0)
                    enumItems.push(x);
            });
        }
        return enumItems;
    },
    toDataSource: function (include) {
        include = include || false;
        var that = this;
        var enumItems = that.toArray(include);
        var ds = new kendo.data.DataSource({
            data: enumItems,
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        Id: {},
                        Name: {},
                        Description: {}
                    }
                }
            }
        });
        ds.fetch();
        return ds;
    }
});

//data
metal.data = {};

//formats
metal.formats.shortDate = 'dd.MM.yyyy';
metal.formats.longDateTime = 'dd-MM-yyyy HH:mm';

//templates
metal.templates.vendor = function (vendor) {
    if (vendor.url)
        return '<a target="_blank" href="' + vendor.url + '">' + vendor.organization + '</a>';
    else
        return '<a href="/vendor/' + vendor.id + '">' + vendor.organization + '</a>';
};

//jquery
$.fn.serializeToObject = function () {
    var data = {};
    this.serializeArray().forEach(function (obj) {
        data[obj.name] = obj.value;
    });
    return data;
};

//Loader
$.fn.showLoader = function (options) {
    kendo.ui.progress($(this), true);
};
$.fn.hideLoader = function (options) {
    kendo.ui.progress($(this), false);
};

//js
Array.prototype.first = function (finder) {
    var foundItems = this.filter(function (item) {
        return finder(item);
    });
    if (foundItems.length > 0)
        return foundItems[0];
    else
        return null;
};
Date.prototype.format = function (format) {
    return kendo.toString(this, format);
};
Date.prototype.toShortDate = function () {
    return this.format(metal.formats.shortDate);
};

String.prototype.toShortDate = function () {
    return (new Date(this)).format(metal.formats.shortDate);
};

//kendo.grid
kendo.ui.Grid.prototype.getEditedDataItem = function () {
    var grid = this;
    return grid.dataItem(grid.table.find('.k-grid-edit-row'));
};
kendo.ui.Grid.prototype.getSelectedDataItem = function () {
    var grid = this;
    return grid.dataItem(grid.select());
};

//STARTUP
$('.m_phone').mask('+7 (000) 000-00-00', { placeholder: "" });
